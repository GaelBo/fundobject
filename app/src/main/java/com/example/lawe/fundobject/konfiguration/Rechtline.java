package com.example.lawe.fundobject.konfiguration;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.example.lawe.fundobject.MainActivity;
import com.example.lawe.fundobject.R;
import com.example.lawe.fundobject.create_object.HomeActivity;

public class Rechtline extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rechtline);
    }
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.rechtline_menu, menu);
        return true;
    }
    public boolean onOptionsItemSelected(MenuItem item) {
        int id= item.getItemId();
        if (id==R.id.action_notifications){
            Intent intent= new Intent(Rechtline.this,Notification.class);
            startActivity(intent);
        }
        else
        if (id==R.id.action_settings){
            Intent intent= new Intent(Rechtline.this,Einstellung.class);
            startActivity(intent);
        }
        else
        if (id==R.id.menu){
            Intent intent= new Intent(Rechtline.this,MainActivity.class);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }
}
