package com.example.lawe.fundobject.konfiguration;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.example.lawe.fundobject.MainActivity;
import com.example.lawe.fundobject.R;
import com.example.lawe.fundobject.create_object.HomeActivity;

import java.util.ArrayList;
import java.util.List;

public class Notification extends AppCompatActivity {

    String[] tsecteur;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);

        List<String> listespinner= new ArrayList<String>();
        listespinner.add("Standard-Klingelton");
        listespinner.add("Keine");
        listespinner.add("Badinerie");
        listespinner.add("Blue");
        listespinner.add("Broken Chord");
        listespinner.add("Humourus");
        listespinner.add("Old Alarm Clock");

        List<String> tsecteur =listespinner= new ArrayList<String>();
        tsecteur.add(" deaktiviert  ");
        tsecteur.add("standardmäßig");
        tsecteur.add("Court");
        tsecteur.add("Long");

        Spinner spinner=(Spinner)findViewById(R.id.spinner_notification);
        Spinner spinner2=(Spinner)findViewById(R.id.spinner_notification2);
        Spinner spinner3=(Spinner)findViewById(R.id.spinner_notification3);
        ArrayAdapter<String>    adapter= new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,listespinner );
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        ArrayAdapter<String>    adapter2= new ArrayAdapter<String>(this,android.R.layout.simple_spinner_dropdown_item,tsecteur );
        adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner2.setAdapter(adapter2);

        ArrayAdapter<String>    adapter3= new ArrayAdapter<String>(this,android.R.layout.simple_spinner_dropdown_item,listespinner );
        adapter3.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner3.setAdapter(adapter3);

    }


















    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.notification_menu, menu);
        return true;
    }
    public boolean onOptionsItemSelected(MenuItem item) {
        int id= item.getItemId();
        if (id==R.id.menu){
            Intent intent= new Intent(Notification.this,MainActivity.class);
            startActivity(intent);
        }
        else
        if (id==R.id.action_rechtlicheHinweise){
            Intent intent= new Intent(Notification.this,Rechtline.class);
            startActivity(intent);
        }
        else
        if (id==R.id.action_settings){
            Intent intent= new Intent(Notification.this,Einstellung.class);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }
}
