package com.example.lawe.fundobject.create_object;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;

import com.example.lawe.fundobject.R;
import com.squareup.picasso.Picasso;

import uk.co.senab.photoview.PhotoViewAttacher;

/*
** https://stackoverflow.com/questions/28139005/how-to-implement-pinch-zoom-in-picasso-library
* Diese Klasse ermöglicht es Ihnen, die Vergrößerung
 * und Verkleinerung eines Bildes zu implementieren.
 *
*  photoViewAttacher = new PhotoViewAttacher(imageZoom);
 */
public class PictureZoom extends AppCompatActivity {

    ImageView imageZoom ;
    String objektName;
    PhotoViewAttacher photoViewAttacher;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_picture_zoom);

        imageZoom = findViewById(R.id.imageZoom);
        objektName = (String)(getIntent().getStringExtra("objektName"));
        getSupportActionBar().setTitle(objektName); // title an page

        Picasso.get()
                //.load("https://parismatch.be/app/uploads/2018/04/Macaca_nigra_self-portrait_large-e1524567086123-1100x715.jpg")
                //.load(new File(objektCurrent.getImage()))
                .load(getIntent().getStringExtra("objektImage"))
                .placeholder(R.drawable.handy1)
                //.error(R.mipmap.add_input_image_muster)
                .noFade().resize(150,150)
                .centerCrop()
                .into(imageZoom);
        photoViewAttacher = new PhotoViewAttacher(imageZoom);

    }
}
