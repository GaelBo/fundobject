package com.example.lawe.fundobject.home_user;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ProgressBar;
import android.widget.SearchView;
import android.widget.TextView;

import com.example.lawe.fundobject.adapters_helper.ImageAdapter;
import com.example.lawe.fundobject.create_object.DetailMyObjektActivity;
import com.example.lawe.fundobject.create_object.HomeActivity;
import com.example.lawe.fundobject.create_object.ObjektErstellenActivity;
import com.example.lawe.fundobject.home_user.HomePageUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.example.lawe.fundobject.R;
import com.example.lawe.fundobject.konfiguration.Einstellung;
import com.example.lawe.fundobject.konfiguration.Notification;
import com.example.lawe.fundobject.konfiguration.Rechtline;

import com.example.lawe.fundobject.models.Objekt;
import com.example.lawe.fundobject.registrierung_anmeldung.*;

import java.util.ArrayList;
import java.util.Locale;

import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.database.Cursor;
import android.widget.TextView;

import com.example.lawe.fundobject.R;
import com.example.lawe.fundobject.adapters_helper.MyListAdapter;
import com.example.lawe.fundobject.adapters_helper.SQLiteHelper;
import com.example.lawe.fundobject.create_object.ObjektDetailActivity;

import java.util.ArrayList;

import static com.facebook.FacebookSdk.getApplicationContext;

public class Fragment_home_page_user_publikation extends Fragment implements ImageAdapter.OnItemClickListener, Filterable{
    private RecyclerView mRecyclerView;
    private ImageAdapter mAdapter;

    private ProgressBar mProgressCircle;
    private  TextView textView_name_user_home;
    private DatabaseReference mDatabaseRef;
    private List<Objekt>mObjekts;
    private List<Objekt>mObjektsFull;
    private List<String>mIdObjekts;
    String idObjekt;
    private  String name_user,email_user;
    SessionUser sessionUser;
    SearchView searchView_user;
    //Vars session
    String emailCurrent;
    int id_user;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        //return super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_home_page_user_publikation, container, false);

        mRecyclerView =(RecyclerView) view.findViewById(R.id.recycler_view_user);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(view.getContext())); // fixme wenn view crash
        mProgressCircle = view.findViewById(R.id.progress_circle_user);
        searchView_user = (SearchView) view.findViewById(R.id.searchView_user);
       // textView_name_user_home= view.findViewById(R.id.textView_name_user_home);

        sessionUser = new SessionUser(getApplicationContext());
        if(sessionUser.isLoggedIn()) {
            // sessionUser = new SessionUser(getApplicationContext());
             sessionUser.checkLogin();
            // get user data from session
            HashMap<String, String> user = sessionUser.getUserDetails();

            // name
              name_user = user.get(SessionUser.KEY_NAME);

            // email
              email_user = user.get(SessionUser.KEY_EMAIL);


        }

        mObjekts = new ArrayList<>(); //weil mit Array ist mor flexibel stadt List<>
        mIdObjekts = new ArrayList<>(); //um Id jede Objekt zu speeichern
        mObjektsFull =new ArrayList<>(); //to search



        mDatabaseRef = FirebaseDatabase.getInstance().getReference("Objekt");
        mDatabaseRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                //okay
                for(DataSnapshot postSnapshot : dataSnapshot.getChildren()){
                    Objekt objekt = postSnapshot.getValue(Objekt.class);
                    idObjekt = postSnapshot.getKey(); //id jeder Objekt

                    String emailTmp = objekt.getId_ersteller();
                    // String key = mDatabaseRef.child("Objekt").push().getKey();
                   // final String objetktID = mIdObjekts.get(position);

                    try {
                        JSONObject tmp = new JSONObject(emailTmp);
                        emailTmp = new String(tmp.getString("email"));
                        //String phone = new String(tmp.getString("phone"));
                    }catch (JSONException j){}

                    if(emailTmp.equals(email_user)){
                        mIdObjekts.add(idObjekt); //ID firebase store use
                        mObjekts.add(objekt);
                        mObjektsFull.add(objekt); // to search
                    }


                }
                // Wenn objekts erstellen, dann create adapter
                displayObjekt(mObjekts);


            }
            // ValueEventListener wird Override hier
            @Override
            public void onCancelled(DatabaseError databaseError) {
                //error fixed when eg Internet or Permission
                Toast.makeText(getContext(), databaseError.getMessage(),Toast.LENGTH_SHORT).show();
                mProgressCircle.setVisibility(View.INVISIBLE);
            }
        });









        return view;
    }

     // Funktionen
    public void displayObjekt( List<Objekt>myObj) {

        // Wenn objekts erstellen, dann create adapter
        mAdapter = new ImageAdapter(getContext(),myObj);

        mRecyclerView.setAdapter(mAdapter);
        // mAdapter.setOnItemClickListener(HomeActivity.this);
        mAdapter.setOnItemClickListener(new ImageAdapter.OnItemClickListener() {

            @Override
            public void onItemClick(int position) {
                Intent intent = new Intent(getContext(),DetailMyObjektActivity.class);
                final String objetktID = mIdObjekts.get(position);

                String phone ="123456";
                intent.putExtra("objektID",objetktID);
                intent.putExtra("objektImage",  mObjekts.get(position).getImage());
                // intent.putExtra("objektType",  mObjekts.get(position).getType); //  fixme  false or true
                intent.putExtra("objektName",  mObjekts.get(position).getName());
                intent.putExtra("objektKategorie",  mObjekts.get(position).getKategorie()).toString();
                //Convertion to String with String.valueOf()
                String dateString = getDateErstellen(mObjekts.get(position).getDate_objekt_erstellen());
                intent.putExtra("objektPrice", String.valueOf( mObjekts.get(position).getPrice()));
                intent.putExtra("objektDate", dateString); // todo
                //Toast.makeText(getApplicationContext(),dateString,Toast.LENGTH_LONG).show();
                intent.putExtra("objektBeschreibung",  mObjekts.get(position).getBeschreibung());
                intent.putExtra("idErsteller",  mObjekts.get(position).getId_ersteller());
                intent.putExtra("Phone",phone  );
                intent.putExtra("objektAdresse",  mObjekts.get(position).getPlz().concat(mObjekts.get(position).getStrasse()));
                intent.putExtra("objektPlz",  mObjekts.get(position).getPlz());
                intent.putExtra("objektStrasse",  mObjekts.get(position).getStrasse());
                intent.putExtra("objektStadt",  mObjekts.get(position).getStadt());
                intent.putExtra("objektDate",  mObjekts.get(position).getDate_objekt_erstellen());

                startActivity(intent);
            }
        });

        mProgressCircle.setVisibility(View.INVISIBLE);

    }


//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.search_menu, menu);
//
//        // to search implementieren
//        MenuItem searchObjekt = menu.findItem(R.id.searchView_user);
//        SearchView searchView = (SearchView) searchObjekt.getActionView();
//        searchView.setImeOptions(EditorInfo.IME_ACTION_DONE); //search another
//
//        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
//            @Override
//            public boolean onQueryTextSubmit(String query) {
//                return false;
//            }
//
//            @Override
//            public boolean onQueryTextChange(String newText) {
//                // mAdapter.getFilter().filter(newText);
//                getFilter().filter(newText);
//                return false;
//            }
//        });
//        return true;
//    }




    public boolean onOptionsItemSelected(MenuItem item) {
        int id= item.getItemId();
        if (id==R.id.action_notifications){
            Intent intent= new Intent(getContext(),Notification.class);
            startActivity(intent);
        }
        else
        if (id==R.id.action_rechtlicheHinweise){
            Intent intent= new Intent(getContext(),Rechtline.class);
            //Intent intent= new Intent(MainActivity.this,ObjektErstellenActivity.class);
            startActivity(intent);
        }
        else
        if (id==R.id.action_settings){
            Intent intent= new Intent(getContext(),Einstellung.class);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onItemClick(int position) {
        Intent intent;
        String phone = null;
        String emailTmp = mObjekts.get(position).getId_ersteller();
        // String key = mDatabaseRef.child("Objekt").push().getKey();
        final String objetktID = mIdObjekts.get(position);

        try {
            JSONObject tmp = new JSONObject(emailTmp);
            emailTmp = new String(tmp.getString("email"));
            phone = new String(tmp.getString("phone"));
        }catch (JSONException j){}
        // Toast.makeText(HomeActivity.this,emailTmp, Toast.LENGTH_LONG).show();
        // Toast.makeText(HomeActivity.this,objetktID, Toast.LENGTH_LONG).show();

        if (emailTmp.equals(emailCurrent)) {
            intent = new Intent(getContext(),DetailMyObjektActivity.class);
        } else {
            // intent = new Intent(HomeActivity.this,DetailMyObjektActivity.class);
            intent = new Intent(getContext(),ObjektDetailActivity.class);
        }
        intent.putExtra("objektID",objetktID);
        intent.putExtra("objektImage",  mObjekts.get(position).getImage());
        // intent.putExtra("objektType",  mObjekts.get(position).getType); //  fixme  false or true
        intent.putExtra("objektName",  mObjekts.get(position).getName());
        intent.putExtra("objektKategorie",  mObjekts.get(position).getKategorie()).toString();
        //Convertion to String with String.valueOf()
        String dateString = getDateErstellen(mObjekts.get(position).getDate_objekt_erstellen());
        intent.putExtra("objektPrice", String.valueOf( mObjekts.get(position).getPrice()));
        intent.putExtra("objektDate", dateString); // todo
        //Toast.makeText(getApplicationContext(),dateString,Toast.LENGTH_LONG).show();
        intent.putExtra("objektBeschreibung",  mObjekts.get(position).getBeschreibung());
        intent.putExtra("idErsteller",  mObjekts.get(position).getId_ersteller());
        intent.putExtra("Phone", phone  );
        intent.putExtra("objektAdresse",  mObjekts.get(position).getPlz().concat(mObjekts.get(position).getStrasse()));
        intent.putExtra("objektPlz",  mObjekts.get(position).getPlz());
        intent.putExtra("objektStrasse",  mObjekts.get(position).getStrasse());
        intent.putExtra("objektStadt",  mObjekts.get(position).getStadt());
        intent.putExtra("objektDate",  mObjekts.get(position).getDate_objekt_erstellen());

        startActivity(intent);
    }

    /*
     **to search implementiren wir Filterable interface
     */
    @Override
    public Filter getFilter() {
        return objektFilter;
    }
    private Filter objektFilter = new Filter() {
        /*
        @constraint ist der ParamCurrent für Sortierung
         */
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<Objekt> filteredList = new ArrayList<>();
            if (constraint == null || constraint.length() == 0) {
                filteredList.addAll(mObjektsFull);
            } else {
                String filterPattern = constraint.toString().toLowerCase().trim();
                // Sortierung nach Name, Kategorie, Plz, Stadt, Strasse
                for (Objekt objekt: mObjektsFull) {
                    if (objekt.getName().toLowerCase().contains(filterPattern) ||
                            objekt.getKategorie().toLowerCase().contains(filterPattern) ||
                            objekt.getPlz().toLowerCase().contains(filterPattern) ||
                            objekt.getStadt().toLowerCase().contains(filterPattern) ||
                            objekt.getStrasse().toLowerCase().contains(filterPattern)) {
                        filteredList.add(objekt);
                    }
                }
            }
            //nach Sortirung return wirs filtererdList durch results variable
            FilterResults results = new FilterResults();
            results.values = filteredList;

            return  results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            //wir muessen jetzt alte LIST löschen,  neu LISTObjekts add
            mObjekts.clear();
            mObjekts.addAll((List) results.values);
            // Toast.makeText(HomeActivity.this, "Sortierung functioniert gut aber Adapter muss refrescht werden ", Toast.LENGTH_LONG).show();
            // mAdapter.notifyDataSetChanged(); //refrech der Adapter // fixme
           // displayObjekt(mObjekts);

        }
    };
    //convertor Date
    public static String getDateErstellen(Date date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "EEE, d MMM yyyy HH:mm", Locale.getDefault());
        return dateFormat.format(date);
    }

    }

