package com.example.lawe.fundobject.registrierung_anmeldung;

import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public abstract class BaseActivity extends AppCompatActivity {


    // --------------------
    // UTILS
    // --------------------

    // La librairie FirebaseUI nous fournit un objet Singleton  FirebaseAuth.getInstance() bien pratique,
    // contenant une méthode nous permettant de récupérer l'utilisateur actuellement connecté à notre application ( getCurrentUser() )
    // et qui renvoi un objet de type FirebaseUser.
    @Nullable
    protected FirebaseUser getCurrentUser(){ return FirebaseAuth.getInstance().getCurrentUser(); }

    //La seconde méthode isCurrentUserLogged() permet,
    // comme son nom l'indique, de savoir si l'utilisateur actuel est connecté ou non.
    protected Boolean isCurrentUserLogged(){ return (this.getCurrentUser() != null); }
}