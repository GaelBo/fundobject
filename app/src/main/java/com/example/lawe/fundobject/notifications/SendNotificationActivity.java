package com.example.lawe.fundobject.notifications;

import com.example.lawe.fundobject.R;
import com.example.lawe.fundobject.models.User;

import java.io.Console;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.facebook.FacebookSdk.getApplicationContext;

public class SendNotificationActivity {

    public   static void sendNotification(String title,String body, String token){
        //Notification when Buchen


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://androidnotificationtutorial.firebaseapp.com/api/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        Api api = retrofit.create(Api.class);
        Call<ResponseBody> call = api.sendNotification(token, title, body);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                System.out.println("alles gut");
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

}
