package com.example.lawe.fundobject.registrierung_anmeldung;

import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import android.widget.Button;
import android.widget.Toast;

import com.example.lawe.fundobject.MainActivity;
import com.example.lawe.fundobject.R;
import com.example.lawe.fundobject.konfiguration.Notification;
import com.example.lawe.fundobject.konfiguration.Rechtline;

public class  Registrationlogin extends AppCompatActivity {


    private SectionsPagerAdapter mSectionsPagerAdapter;

    private ViewPager mViewPager;
    private Button speicher;
    private  String object_page;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
         super.onCreate(savedInstanceState);
         setContentView(R.layout.activity_registrationlogin);

         Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
         setSupportActionBar(toolbar);
         mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
         mViewPager = (ViewPager) findViewById(R.id.container);
         mViewPager.setAdapter(mSectionsPagerAdapter);
         TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
         mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
         tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPager));
        String object_page1=getIntent().getStringExtra("object_page");
        if (object_page1==null){
        }else{
            object_page=object_page1;
        }
}

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_home_user, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id= item.getItemId();
        if (id==R.id.action_notifications){
            Intent intent= new Intent(Registrationlogin.this,Notification.class);
            startActivity(intent);
        }
        else
        if (id==R.id.action_rechtlicheHinweise){
            Intent intent= new Intent(Registrationlogin.this,Rechtline.class);
            startActivity(intent);
        }
        else
        if (id==R.id.menu){
            Intent intent= new Intent(Registrationlogin.this,MainActivity.class);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            //Fragment fragment=null;
            switch (position){
                case 0:{
                    LoginFragment fragment= new LoginFragment();

                    Bundle bundle = new Bundle();
                    bundle.putString("object_page",object_page);
                    fragment.setArguments(bundle);

                    return fragment;
                   // break;
                }
                case 1:{
                    RegistrierungFragment fragment = new RegistrierungFragment();
                    //break;
                    Bundle bundle = new Bundle();
                    bundle.putString("object_page",object_page);
                    fragment.setArguments(bundle);

                    return fragment;
                }
                default:
                    return null;

            }
            //return fragment;
        }

        @Override
        public int getCount() {

            return 2;
        }
    }





}
