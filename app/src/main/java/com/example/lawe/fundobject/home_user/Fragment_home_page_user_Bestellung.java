package com.example.lawe.fundobject.home_user;


import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.lawe.fundobject.R;
import com.example.lawe.fundobject.adapters_helper.ImageAdapter;
import com.example.lawe.fundobject.create_object.DetailMyObjektActivity;
import com.example.lawe.fundobject.create_object.ObjektDetailActivity;
import com.example.lawe.fundobject.models.Objekt;
import com.example.lawe.fundobject.models.User;
import com.example.lawe.fundobject.registrierung_anmeldung.DBManager;
import com.example.lawe.fundobject.registrierung_anmeldung.SessionUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.example.lawe.fundobject.home_user.Fragment_home_page_user_publikation.getDateErstellen;
import static com.facebook.FacebookSdk.getApplicationContext;

public class Fragment_home_page_user_Bestellung extends Fragment {
    public static final String TAG = " fragment_login";
    DBManager db;
    //Bloc sauvegarde des Daten du user: SharePreference declarer outside OnCreate
    EditText email_login, pwd_login;
    Button login_login;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    Boolean savelogin;
    CheckBox checkbox_login;
    private RecyclerView mRecyclerView;
    private ImageAdapter mAdapter;
    private  String resultat_user_firebase = "null";
    private ProgressBar mProgressCircle;
    private TextView textView_name_user_home;
    private DatabaseReference mDatabaseRef,mDatabaseRef_user;
    private List<Objekt> mObjekts;
    private List<Objekt>mObjektsFull;
    private List<String>mIdObjekts;
    String idObjekt;
    private  String name_user,email_user,  userId_firebase;
    SessionUser sessionUser;
    SearchView searchView_user;
    //Vars session
    String emailCurrent;
    int id_user;
    private User user_recupere_firebase=null;
    private List<User> listeUser ;
    //FIN Bloc sauvegarde des Donnees du user: SharePreferences

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        //return super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_home_page_user_bestellung, container, false);


        mRecyclerView =(RecyclerView) view.findViewById(R.id.recycler_view_user2);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(view.getContext())); // fixme wenn view crash
        mProgressCircle = view.findViewById(R.id.progress_circle_user2);
        searchView_user = (SearchView) view.findViewById(R.id.searchView_user2);
        // textView_name_user_home= view.findViewById(R.id.textView_name_user_home);

        sessionUser = new SessionUser(getApplicationContext());
        if(sessionUser.isLoggedIn()) {
            // sessionUser = new SessionUser(getApplicationContext());
            sessionUser.checkLogin();
            // get user data from session
            HashMap<String, String> user = sessionUser.getUserDetails();

            // name
            name_user = user.get(SessionUser.KEY_NAME);

            // email
            email_user = user.get(SessionUser.KEY_EMAIL);

            try {
                JSONObject info_user=new JSONObject(name_user);
                   userId_firebase=info_user.getString("userId_firebase");
                name_user=info_user.getString("name");
            } catch (JSONException e) {
                e.printStackTrace();
            }


        }

        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        mDatabaseRef_user = FirebaseDatabase.getInstance().getReference("user");

        mDatabaseRef_user.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                //okay
                for(DataSnapshot postSnapshot : dataSnapshot.getChildren()){
                    user_recupere_firebase = postSnapshot.getValue(User.class);
                    if(userId_firebase.equals(postSnapshot.getKey())){

                        //id jeder Objekt
                        User user_recupere_firebase_ = postSnapshot.getValue(User.class);

                            User u=postSnapshot.getValue(User.class);

                            if(u.getBestellung().equals("null")){

                                resultat_user_firebase ="null";

                            }else{
                                try {
                                    JSONArray liste_bestellungen= new JSONArray(u.getBestellung());
                                    int index=0;

                                    while(index<liste_bestellungen.length()){

                                        JSONObject bestellung=  liste_bestellungen.getJSONObject(index);
                                        String id_objekt_bestellt=bestellung.getString("id_objekt_bestellt");

                                        if (bestellung.getString("id_objekt_bestellt").equals(id_objekt_bestellt)){

                                                   Toast.makeText(getApplicationContext(), " You already ordered it.",Toast.LENGTH_LONG).show();
                                        }


                                        index++;
                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }


                        //######################################################################################################################

                    }

                }

                // Wenn objekts erstellen, dann create adapter



            }



            @Override
            public void onCancelled(DatabaseError databaseError) {
                //error fixed when eg Internet or Permission

            }
        });
        //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


        //############################################################################################################
        mObjekts = new ArrayList<>(); //weil mit Array ist mor flexibel stadt List<>
        mIdObjekts = new ArrayList<>(); //um Id jede Objekt zu speeichern
        mObjektsFull =new ArrayList<>(); //to search
        mDatabaseRef = FirebaseDatabase.getInstance().getReference("Objekt");
        mDatabaseRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                //okay
                for(DataSnapshot postSnapshot : dataSnapshot.getChildren()){
                    Objekt objekt = postSnapshot.getValue(Objekt.class);
                    idObjekt = postSnapshot.getKey(); //id jeder Objekt
                    String emailTmp = objekt.getId_ersteller();
                    try {
                        JSONObject tmp = new JSONObject(emailTmp);
                        emailTmp = new String(tmp.getString("email"));
                        //String phone = new String(tmp.getString("phone"));
                    }catch (JSONException j){}
                    if(emailTmp.equals(email_user)){
                    //if(belong_to_commande_user(userId_firebase,idObjekt)){
                        mIdObjekts.add(idObjekt); //ID firebase store use
                        mObjekts.add(objekt);
                        mObjektsFull.add(objekt); // to search
                    }
                }
                displayObjekt(mObjekts);
            }
            public void displayObjekt( List<Objekt>myObj) {
                mAdapter = new ImageAdapter(getContext(),myObj);
                mRecyclerView.setAdapter(mAdapter);
                // mAdapter.setOnItemClickListener(HomeActivity.this);
                mAdapter.setOnItemClickListener(new ImageAdapter.OnItemClickListener() {

                    @Override
                    public void onItemClick(int position) {
                        Intent intent = new Intent(getContext(),DetailMyObjektActivity.class);
                        final String objetktID = mIdObjekts.get(position);

                        String phone ="123456";
                        intent.putExtra("objektID",objetktID);
                        intent.putExtra("objektImage",  mObjekts.get(position).getImage());
                        // intent.putExtra("objektType",  mObjekts.get(position).getType); //  fixme  false or true
                        intent.putExtra("objektName",  mObjekts.get(position).getName());
                        intent.putExtra("objektKategorie",  mObjekts.get(position).getKategorie()).toString();
                        //Convertion to String with String.valueOf()
                        String dateString = getDateErstellen(mObjekts.get(position).getDate_objekt_erstellen());
                        intent.putExtra("objektPrice", String.valueOf( mObjekts.get(position).getPrice()));
                        intent.putExtra("objektDate", dateString); // todo
                        //Toast.makeText(getApplicationContext(),dateString,Toast.LENGTH_LONG).show();
                        intent.putExtra("objektBeschreibung",  mObjekts.get(position).getBeschreibung());
                        intent.putExtra("idErsteller",  mObjekts.get(position).getId_ersteller());
                        intent.putExtra("Phone",phone  );
                        intent.putExtra("objektAdresse",  mObjekts.get(position).getPlz().concat(mObjekts.get(position).getStrasse()));
                        intent.putExtra("objektPlz",  mObjekts.get(position).getPlz());
                        intent.putExtra("objektStrasse",  mObjekts.get(position).getStrasse());
                        intent.putExtra("objektStadt",  mObjekts.get(position).getStadt());
                        intent.putExtra("objektDate",  mObjekts.get(position).getDate_objekt_erstellen());

                        startActivity(intent);
                    }
                });

                mProgressCircle.setVisibility(View.INVISIBLE);

            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
                //error fixed when eg Internet or Permission
                Toast.makeText(getContext(), databaseError.getMessage(),Toast.LENGTH_SHORT).show();
                mProgressCircle.setVisibility(View.INVISIBLE);
            }
        });
        //#############################################################################################################################

        return view;
    }


    public boolean belong_to_commande_user(String userId_firebase,String idObjekt){
        Toast.makeText(getApplicationContext(),idObjekt+" cesaire"+userId_firebase,Toast.LENGTH_LONG).show();

        return true;
    }



    //Gael
//Bloc share Preference Einllogen

    //End login(); Bloc share Preference

}
