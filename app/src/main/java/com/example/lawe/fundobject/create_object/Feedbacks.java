package com.example.lawe.fundobject.create_object;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.lawe.fundobject.R;

public class Feedbacks extends Fragment {

    public Feedbacks(){};

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View FeedbacksPage = inflater.inflate(R.layout.feedbacks, container, false);
        return FeedbacksPage;
    }
}
