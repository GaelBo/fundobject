package com.example.lawe.fundobject.registrierung_anmeldung;
import com.example.lawe.fundobject.R;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebView;

public class FacebookConnectionActivity extends AppCompatActivity {

    WebView myWebView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_facebook_connection);

        myWebView = (WebView) findViewById(R.id.webView_facebook);
        myWebView.loadUrl("http://www.facebook.com");
       // setContentView(myWebView);


    }
}
