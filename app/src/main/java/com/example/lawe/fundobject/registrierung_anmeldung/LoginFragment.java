package com.example.lawe.fundobject.registrierung_anmeldung;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.example.lawe.fundobject.models.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import com.example.lawe.fundobject.R;
import com.example.lawe.fundobject.create_object.HomeActivity;
import com.example.lawe.fundobject.create_object.ObjektErstellenActivity;
import com.example.lawe.fundobject.home_user.Fragment_home_page_user_Bestellung;
import com.example.lawe.fundobject.home_user.Fragment_home_page_user_publikation;
import com.example.lawe.fundobject.home_user.HomePageUser;
import com.example.lawe.fundobject.connection_internet_checking.connection_internet_checking;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.Collections;
import java.util.Set;

import static android.content.Context.MODE_PRIVATE;
import static com.facebook.FacebookSdk.getApplicationContext;

public class LoginFragment extends Fragment {
    public static final String TAG = " fragment_login";
    //DBManager db;
    DBManager db;
    SessionUser session;

    //Bloc sauvegarde des Daten du user: SharePreference declarer outside OnCreate
    EditText email_login, pwd_login;
    Button login_login;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    Boolean savelogin;
    CheckBox checkbox_login;
    String user_email;
    String passwrd,message="";
    String nulle="null";
    int id_user=0;
    private FirebaseAuth auth;
    SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0);



    //FIN Bloc sauvegarde des Donnees du user: SharePreferences

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        //return super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_login, container, false);


        /***********************************************/
        //Bloc sauvegarde des Daten du user: SharePreferences
        email_login=(EditText)view.findViewById(R.id.email_login);
        pwd_login=(EditText)view.findViewById(R.id.pwd_login);
        login_login=(Button)view.findViewById(R.id.login_login);

        /*editor = pref.edit();
        email_login.setText(pref.getString("email",null));
        pwd_login.setText(pref.getString("passwrd",null));
        editor.putInt("id_user", id_user);
        editor.putString("password",passwrd);
        editor.putString("name",name);
        editor.putString("email",user_email);
        editor.commit();*/
        //sharedPreference() need a context to be accessed
        //email_login.setText(sharedPreferences.getString("username", null));
        //pwd_login.setText(sharedPreferences.getString("password", null));

         sharedPreferences = getContext().getSharedPreferences("loginref",MODE_PRIVATE);
         checkbox_login=(CheckBox)view.findViewById(R.id.checkBox_login);
         editor=sharedPreferences.edit();
        savelogin=sharedPreferences.getBoolean("savelogin",true);
    /*    if(savelogin==true) {
            email_login.setText(sharedPreferences.getString("username", null));
            pwd_login.setText(sharedPreferences.getString("password", null));
        }else{

        }
        */
        auth=FirebaseAuth.getInstance();

        db= new DBManager(getContext());
        login_login.setOnClickListener(new View.OnClickListener() {
            @Override    public void onClick(View v) {
                user_email = email_login.getText().toString();
                passwrd = pwd_login.getText().toString();
                String resultat_="",name="";
                //connection width Firebase
                connection_internet_checking  connection_internet=new connection_internet_checking(getContext());
                ///******************************************************************
                if(connection_internet.isConnected()){
                    try {

                        ////////////////////////////////////7\\\\\\\\\\\\\\\\\\\\\\\\\\\\
                        auth.signInWithEmailAndPassword(user_email,passwrd)
                                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                                    @Override
                                    public void onComplete(@NonNull Task<AuthResult> task) {
                                        if (task.isSuccessful()){
                                            message ="success width Firebase";
                                        }
                                    }
                                });
                        ///////////////////////////////////7\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }else{
                    message="you don't have an internet connection ";

                }

                    //connection width the local database
                JSONObject resultat= db.verifiertExisteUser(user_email,passwrd);
                if(resultat!=null){

                try {
                    resultat_=resultat.getString("id_test");
                    message = message+resultat.getString("message");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if(resultat_.equals(nulle)){
                }else {
                    try {
                        id_user = resultat.getInt("id");
                        name = "{\"name\":\""+resultat.getString("name")+"\"," +
                                "\"userId_firebase\":\""+resultat.getString("userId_firebase")+"\"}";

                       // id = new Integer(resultat.getString("id"));

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
               if(resultat_.equals(nulle)){
                }else{
                    user_email = email_login.getText().toString();
                    Bundle bundle= getArguments();
                    String  object_page1=bundle.getString("object_page");
                    Intent i;
                     if (object_page1==null){
                          i= new Intent(getContext(),HomePageUser.class);
                    }else{
                          i= new Intent(getContext(),ObjektErstellenActivity.class);
                    }

                   session = new SessionUser(getApplicationContext());
                    session.createLoginSession(id_user,name, user_email);

                   i.putExtra("id_user",id_user);
                    i.putExtra("name",name);
                    i.putExtra("email",user_email);
                   startActivity(i);

                   try {
                       this.finalize();
                   } catch (Throwable throwable) {
                       throwable.printStackTrace();
                   }

                }
                Toast.makeText(getActivity(),message, Toast.LENGTH_SHORT).show();


                }else{
                    if(message.equals("success width Firebase")){
                        User u = new User();
                        u.setName("you are not connected on your phone");
                        u.setEmail(user_email);
                        u.setPassword(passwrd);
                        boolean insertDatabase=db.addData(u);
                        session = new SessionUser(getApplicationContext());
                        session.createLoginSession(id_user,name, user_email);
                    }else{

                        Toast.makeText(getActivity(),"You should register first", Toast.LENGTH_SHORT).show();
                    }

                }
            }
        });




        /****************************************************/

        return view;
    }




    //Gael
//Bloc share Preference Einllogen
    public JSONObject login(String user_email1,String  passwrd1){
         user_email = user_email1;
         passwrd = passwrd1;


        JSONObject Verifiet_exist=db.verifiertExisteUser(user_email,passwrd);

        int index=0;
        JSONObject resultat = null;
             try {
                JSONObject object=  Verifiet_exist;

                String id_test=new String(object.getString("id_test"));
                String name=new String(object.getString("name"));
                String message=new String(object.getString("message"));
                String nulle="null";
                if(id_test.equals(nulle)){
                    Toast.makeText(getActivity(),message
                            , Toast.LENGTH_SHORT).show();
                    String info="{\"id\":\"null\", " +
                            "\"name\":\"null\"," +
                            "\"resultat\":\"null\"," +
                            "\"message\":\"null\"}";
                    resultat= new JSONObject(info);

                }else{
                    int id=new Integer(object.getString("id"));
                    Toast.makeText(getActivity(),message
                            , Toast.LENGTH_SHORT).show();
                    String info="{\"id\":\""+id+"\", " +
                                "\"name\":\""+name+"\"," +
                                "\"resultat\":\"ok\"," +
                                "\"message\":\""+message+"\"}";
                    resultat= new JSONObject(info);


                }
            }catch (JSONException e){
                Toast.makeText(getActivity(),"connection failur   with server"
                        , Toast.LENGTH_SHORT).show();
            }


         return  resultat;

    }
 //End login(); Bloc share Preference

}
