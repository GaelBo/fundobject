package com.example.lawe.fundobject.registrierung_anmeldung;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.lawe.fundobject.R;
import com.example.lawe.fundobject.create_object.HomeActivity;
import com.example.lawe.fundobject.create_object.ObjektErstellenActivity;
import com.example.lawe.fundobject.home_user.HomePageUser;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.example.lawe.fundobject.models.User;
import com.example.lawe.fundobject.connection_internet_checking.connection_internet_checking;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;

import static com.facebook.FacebookSdk.getApplicationContext;

public class RegistrierungFragment extends Fragment  {
    View view = null;
    public static final String TAG=" fragment_registrierung";
    SessionUser session;
    //backend with firebase
    private  String token_user  ;
    private FirebaseAuth firebaseAuth;
    private DatabaseReference reference;
    private String string_txtEmailAdresse,string_txtPassword;

    private  Button button_speichern_registrierung;
    private  EditText name,email,pswd,pswd2;
    private boolean resultat_registrierung_filebase;
    LoginFragment loginFragment;
    int id_user;
    DBManager db;
    private  String  userId_firebase="null";
    Context context;
    boolean resultat_registrierung=false;
    //@Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view=inflater.inflate(R.layout.fragment_registrierung,container,false);

        name=(EditText)view.findViewById(R.id.name_registrierung);
        email=(EditText)view.findViewById(R.id.email_registrierung);
        pswd=(EditText)view.findViewById(R.id.pswd_registrierung);
        pswd2=(EditText)view.findViewById(R.id.pswd_registrierung2);

        button_speichern_registrierung=(Button)view.findViewById(R.id.button_speichern_registrierung);
        firebaseAuth=FirebaseAuth.getInstance();
        context= getContext();
        db= new DBManager(getContext());


        button_speichern_registrierung.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View v) {
                // Authentification with sqlite
                if (name.length()!=0  & email.length()!=0  & pswd.length()!=0 & pswd.getText().toString().equals(pswd2.getText().toString()) & pswd.getText().toString().length()>=6 ){

                    // db.deleteAll();
                    User u = new User();
                    u.setName(name.getText().toString());
                    u.setEmail(email.getText().toString());
                    u.setPassword(pswd.getText().toString());
                    connection_internet_checking  connection_internet=new connection_internet_checking(getContext());
                    ///******************************************************************
                    if(connection_internet.isConnected()){

                         //*******************************************************************
                        try {

                            ////////////////////////////////////7\\\\\\\\\\\\\\\\\\\\\\\\\\\\
                            //Authentification with firebase
                          String userId_firebase= registrieren(u);
                          if (userId_firebase!="null"){
                              u.setUserId_firebase(userId_firebase);

                              boolean insertDatabase=db.addData(u);
                              if (insertDatabase==true ) {
                                  Toast.makeText(getActivity(), "Register success width sqlite and filebase!", Toast.LENGTH_SHORT).show();

                                  resultat_registrierung=true;
                              } else {
                                  Toast.makeText(getActivity(), "connection error width your phone reinstall the application" ,
                                          Toast.LENGTH_LONG).show();


                              }
                          }else{
                              Toast.makeText(getActivity(), "connection error width the server" ,
                                      Toast.LENGTH_LONG).show();
                          }
                            // Sqlite

                            ///////////////////////////////////7\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        //Toast.makeText(getActivity(), "Register success  width a connection!", Toast.LENGTH_SHORT).show();

                    }else{


                        //*******************************************************************
                        try {

                            ////////////////////////////////////7\\\\\\\\\\\\\\\\\\\\\\\\\\\\
                            boolean insertDatabase=db.addData(u);

                            if (insertDatabase==true) {
                                Toast.makeText(getActivity(), "Register success but your are not connected in internet", Toast.LENGTH_SHORT).show();

                                resultat_registrierung=true;
                            } else {
                                Toast.makeText(getActivity(), "connection error" ,
                                        Toast.LENGTH_LONG).show();
                            }

                            ///////////////////////////////////7\\\\\\\\\\\\\\\\\\\\\\\\\\\\\



                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    if(resultat_registrierung){

                        int  id_user= db.search_id_user(email.getText().toString(),pswd.getText().toString());


                        Bundle bundle= getArguments();
                        String  object_page1=bundle.getString("object_page");
                        Intent intent_kont;
                        if (object_page1==null){
                             intent_kont = new Intent(getContext(), HomePageUser.class);
                            intent_kont.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK); // fixme when crash
                        }else{
                            intent_kont= new Intent(getContext(),ObjektErstellenActivity.class);
                            intent_kont.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        }
                        session = new SessionUser(getApplicationContext());
                        session.createLoginSession(id_user,u.name, u.email);


                        startActivity(intent_kont);
                    }

                }else{
                    if(pswd.getText().toString().equals(pswd2.getText().toString())){

                        if(pswd.getText().toString().length()<6){
                            Toast.makeText(getActivity(), "password too short,less than six letters",
                                    Toast.LENGTH_LONG).show();
                        }else {
                            Toast.makeText(getActivity(), "Alle musst ausgefühlt werden",
                                    Toast.LENGTH_LONG).show();
                        }
                    }else{

                            Toast.makeText(getActivity(), "passwords do not agree",
                                    Toast.LENGTH_LONG).show();

                    }

                    // display("Liste von Usern","Alle musst ausgefühlt werden");
                }

            }
        });

        return  view;

    } //end view Oncreate

    //firebase
    public String registrieren(final User u) {

        // recuperation of the token from the user

        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if(task.isSuccessful()){
                            token_user =task.getResult().getToken();
                        }else{
                            token_user=task.getException().getMessage();
                        }
                    }
                });
        final ProgressDialog progressDialog = ProgressDialog.show(getActivity(), "Please wait...", "Proccessing ...", true);
        (firebaseAuth.createUserWithEmailAndPassword(u.getEmail(), u.getPassword()))
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        progressDialog.dismiss();
                        if (task.isSuccessful()) {

                            FirebaseUser firebaseUser= firebaseAuth.getCurrentUser();
                            String userId=firebaseUser.getUid();
                            reference=FirebaseDatabase.getInstance().getReference("user").child(userId);
                            u.setId(userId);
                            u.setToken_user(token_user);

                            reference.setValue(u).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (task.isSuccessful()){
                                          userId_firebase=reference.getKey();

                                        Toast.makeText(getActivity(), "succesfull "+userId_firebase+" registration", Toast.LENGTH_LONG).show();
                                    }else{
                                        Toast.makeText(getActivity(),"error registration", Toast.LENGTH_LONG).show();
                                    }
                                }
                            });
                      // Toast.makeText(getActivity(), "Einloggen ist gut gelaufen ", Toast.LENGTH_LONG).show();
                              // Intent i = new Intent(getActivity(), HomePageUser.class);
//                            //i.putExtra("Email",firebaseAuth.getCurrentUser().getEmail());
//                             i.putExtra("Email",string_txtEmailAdresse);
//                            // i.putExtra("password",string_txtPassword); //für cookie
//                            startActivity(i);

                        } else {
                            Log.e("ERROR", task.getException().toString());
                            Toast.makeText(getActivity(), task.getException().getMessage(), Toast.LENGTH_LONG).show();

                        }
                    }
                });
        return userId_firebase ;

    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    Intent intent1 = new Intent(RegistrierungFragment.this.context, HomeActivity.class);
                    startActivity(intent1);
                    return  true;
                case R.id.navigation_erstellen:
                    Intent intent2 = new Intent(RegistrierungFragment.this.context, ObjektErstellenActivity.class);
                    //Intent intent = new Intent(MainActivity.this, ObjektErstellenActivity.class);
                    startActivity(intent2);
                    return  true;
                case R.id.navigation_konto:
                    Intent intent_kont = new Intent(RegistrierungFragment.this.context, Registrationlogin.class);
                    startActivity(intent_kont);
                    return  true;
            }
            return false;
        }
    };
    /*
    private String List_allUser_undInfo() {
        Log.d(TAG, "populateListView: Displaying data in the ListView.");

        //get the data and append to a list
        Cursor data = db.getData();
        ArrayList<String> listData = new ArrayList<>();
        ArrayList<String> liste_tmp = new ArrayList<>();

        while(data.moveToNext()){
            String tmp=data.getString(1);
            liste_tmp=  new ArrayList<String>(Arrays.asList(tmp.split(",")));

            listData.add(liste_tmp.get(0) ); // dies ist den Name
            listData.add(liste_tmp.get(1) ); // dies ist die Email adresse
            listData.add(liste_tmp.get(2) ); // dies ist das Password

        }

        return  listData.toString();

    }

    private String User_Info() {
        Log.d(TAG, "populateListView: Displaying data in the ListView.");

        //get the data and append to a list
        Cursor data = db.getData();
        ArrayList<String> listData = new ArrayList<>();
        ArrayList<String> liste_tmp = new ArrayList<>();

        while(data.moveToNext()){
            String tmp=data.getString(1);
            liste_tmp=  new ArrayList<String>(Arrays.asList(tmp.split(",")));

            listData.add(liste_tmp.get(0) ); // dies ist den Name
            listData.add(liste_tmp.get(1) ); // dies ist die Email adresse
            listData.add(liste_tmp.get(2) ); // dies ist das Password

        }
        // dies Return gibt uns die erste Namee der Liste
        return  listData.toString();

    }
    */



}
