package com.example.lawe.fundobject.api;

import com.example.lawe.fundobject.models.Objekt;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.Date;

/*
* Cette classe aura pour rôle principal de référencer de manière statique
* les différentes requêtes réseau CRUD concernant la Collection objekt. Pour cela, nous allons utiliser le
* SDK de Firebase et notamment l'objet FirebaseFirestore via son singleton  FirebaseFirestore.getInstance().
* */
public class ObjektHelper {

    private static final String COLLECTION_NAME = "objekt";

    // --- COLLECTION REFERENCE ---

    public static CollectionReference getObjektCollection(){
        //FirebaseFirestore via son singleton  FirebaseFirestore.getInstance()
        return FirebaseFirestore.getInstance().collection(COLLECTION_NAME);
    }

    // --- CREATE ---
//Task  permet de réaliser facilement des appels asynchrones. Pas besoin de créer d'AsyncTask ! et plus haut le singleton
//    public static Task<Void> createObjekt(long id_objekt, String name, String kategorie, boolean type, boolean zustand, float price, String beschreibung, String plz, String strasse, String stadt, long id_ersteller, long[] list_id_besteller, String uri_image, Date date_objekt_erstellen) {
//        Objekt objektToCreate = new Objekt(id_objekt,name,kategorie,type,zustand,price,beschreibung,plz,strasse,stadt,id_ersteller,list_id_besteller, uri_image,  date_objekt_erstellen);
//        long id_obj= ((long)id_objekt);
//        return ObjektHelper.getObjektCollection().document(String.valueOf(id_obj)).set(objektToCreate);
//    }

    // --- GET ---

    public static Task<DocumentSnapshot> getObjekt(long id_objekt){
        return ObjektHelper.getObjektCollection().document(String.valueOf(id_objekt)).get();
    }

    // --- UPDATE ---

//    public static Task<Void> updateObjekt(Objekt obj)
//    {
//        return ObjektHelper.getObjektCollection().document(String.valueOf(obj.getId_objekt())).update("name", obj.getName(),
//                "kategorie",obj.getKategorie(),
//                "istype",obj.isType(),
//                "isZustand",obj.isZustand(),
//                "price",obj.getPrice(),
//                "beschreibung",obj.getBeschreibung());
//                //etc...
//    }

//    public static Task<Void> updateIsMentor(String uid, Boolean isMentor) {
//        return ObjektHelper.getObjektCollection().document(uid).update("isMentor", isMentor);
//    }

    // --- DELETE ---

    public static Task<Void> deleteObjekt(long id_objekt) {
        return ObjektHelper.getObjektCollection().document(String.valueOf(id_objekt)).delete();
    }

}
