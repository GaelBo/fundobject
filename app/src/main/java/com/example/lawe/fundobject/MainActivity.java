package com.example.lawe.fundobject;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;
import android.content.Intent;
import android.media.RingtoneManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.lawe.fundobject.create_object.HomeActivity;
import com.example.lawe.fundobject.create_object.ObjektErstellenActivity;
import com.example.lawe.fundobject.home_user.HomePageUser;
import com.example.lawe.fundobject.konfiguration.Notification;
import com.example.lawe.fundobject.registrierung_anmeldung.FacebookConnectionActivity;
import com.example.lawe.fundobject.registrierung_anmeldung.LoginFragment;
import com.example.lawe.fundobject.registrierung_anmeldung.Registrationlogin;
import com.example.lawe.fundobject.registrierung_anmeldung.RegistrierungFragment;
import com.example.lawe.fundobject.registrierung_anmeldung.SessionUser;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.messaging.FirebaseMessaging;

import java.util.HashMap;

public class MainActivity extends AppCompatActivity {

    Button btnHome,btnConnection, btnFacebook;
    TextView textViewToken;
    SessionUser sessionUser;

    //Notification CHANNEL erstellen
    public static final String CHANNEL_ID = "fundobject_TEAM";
    private static final String CHANNEL_NAME = "Gael & Cesaire";
    private static final String CHANNEL_DESC = "fundobject_TEAM Notifications";


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        btnHome=(Button)findViewById(R.id.btnHome);
        btnConnection=(Button)findViewById(R.id.btnConnection);
        btnFacebook=(Button)findViewById(R.id.btnFacebook);
        textViewToken = (TextView)findViewById(R.id.or); // for example


        sessionUser = new SessionUser(getApplicationContext());
        if(sessionUser.isLoggedIn()) {

            Intent intent= new Intent(this,HomeActivity.class);
            startActivity(intent);

        }




        btnHome.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                home_click(v);
            }
        });

        btnConnection.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                connectionRegisterLogin(v);
            }
        });
//        btnFacebook.setOnClickListener(new View.OnClickListener(){
//
//            @Override
//            public void onClick(View v) {
//                facebookAPI(v);
//
//            }
//        });

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
           NotificationChannel channel = new NotificationChannel(CHANNEL_ID, CHANNEL_NAME, NotificationManager.IMPORTANCE_DEFAULT);
           channel.setDescription(CHANNEL_DESC);
           NotificationManager manager = getSystemService(NotificationManager.class);
           manager.createNotificationChannel(channel);
        }
       // Notification Subscribing to Topic
       // FirebaseMessaging.getInstance().subscribeToTopic("updates");

        // Notifikation to one Divice specifique: TOKEN we need FCM Farebase Cloud Messeging
//        FirebaseInstanceId.getInstance().getInstanceId(); //return one Tast<>
        FirebaseInstanceId.getInstance().getInstanceId().
                addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if(task.isSuccessful()){

                            String token = task.getResult().getToken();
                           // textViewToken.setText("Token : " +token);
                        } else {
                           // textViewToken.setText("no Token fund : " +task.getException().getMessage());
                        }
                    }
                });




        // Notifikation bei Clic
//        btnFacebook.setOnClickListener(new View.OnClickListener(){
//
//            @Override
//            public void onClick(View v) {
//                displayNotification();
//            }
//        });


    }

    public void home_click(View view){
        Intent i=new Intent(MainActivity.this,HomeActivity.class);
        startActivity(i);
    }
    public void connectionRegisterLogin(View view){
        Intent i=new Intent(MainActivity.this,Registrationlogin.class);
        startActivity(i);
    }

    public void facebookAPI(View view){
        Intent i=new Intent(MainActivity.this,FacebookConnectionActivity.class);

        startActivity(i);
    }


}