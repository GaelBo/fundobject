package com.example.lawe.fundobject.create_object;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.example.lawe.fundobject.MainActivity;
import com.example.lawe.fundobject.R;
import com.example.lawe.fundobject.adapters_helper.SQLiteHelper;
import com.example.lawe.fundobject.models.Objekt;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.StorageTask;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.StringTokenizer;

import static android.app.Activity.RESULT_OK;
import static android.provider.Telephony.Mms.Part.TEXT;


public class EditMyObjektActivity extends AppCompatActivity {

    // Write a message to the database realtime
    //FirebaseDatabase database = FirebaseDatabase.getInstance();
    private DatabaseReference mDatabaseRef;
    private StorageReference mStorageRef;
    private StorageTask mObjektTask; //und nur ein Objekt zu speichern beim mehr clicks add
    // Access a Cloud Firestore instance from your Activity6
    FirebaseFirestore db_firestore = FirebaseFirestore.getInstance();

    SQLiteDatabase sqLiteDatabaseObj;
    SQLiteHelper dbHelper; //vllt zu löschen
    EditText editTextName,editTextPrice,editTextBeschreibung, editTextPlz,editTextStasse,
            editTextStadt, editTextIdErsteller,editTextListBesteller, editTextPhone;
    Date dateErstellen;
    EditText editTextImageAdresse; //chemin uri de  l image //fixme todo
    RadioButton radioButtonType;
    Spinner spinnerKategorie;
    Boolean EditTextEmptyHold, booleanEtat;
    //////////////////////////////////////////////////////////
    String NameHolder;
    String KategorieHolder;
    String PriceHolder;
    String BeschreibungHolder;
    String SQLiteDataBaseQueryHolder;
    boolean TypeHolder;
    String EtatHolder;
    String PlzHolder;
    String StrasseHolder;
    String StadtHolder;
    String IdErstellerHolder; // Email/Phone
    String ListIdBestellerHolder; //EmailBestellers
    String PhoneHolder;
    String ImageAdresseHolder;
    Button  btnSaveNew, btnAbbrechen;
    RadioGroup radioGroup;
    ProgressBar mProgressBar;
    int selectedId_edit; //für radiogroud select elt
    //image tuto: saving Retrieving images from SQLite Database in Android Part 1
    //https://www.youtube.com/watch?v=X7T0g5kBYJk
    private ImageView mImageView;
    private static final int SELECT_PHOTO = 1;
    private static final int CAPTURE_PHOTO = 2;
    private Uri mImageUri;

    private ProgressDialog progressBar;
    private int progressBarStatus=0;
    private Handler progressBarHandler=new Handler();
    private boolean hasImageChanged=false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_my_objekt);

        btnSaveNew = (Button)findViewById(R.id.edit_btn_add_objekt);
        btnAbbrechen = (Button)findViewById(R.id.edit_btn_abbrechen);
        mProgressBar = (ProgressBar)findViewById(R.id.edit_input_progress_bar);

        editTextName = (EditText)findViewById(R.id.edit_input_name);
        spinnerKategorie=(Spinner)findViewById(R.id.edit_input_spinner_kategorie);
        radioGroup=(RadioGroup)findViewById(R.id.edit_group);
        selectedId_edit = radioGroup.getCheckedRadioButtonId();
        radioButtonType = (RadioButton) findViewById(selectedId_edit);
        booleanEtat=true; //Objekt ist beim Erstellen direkt verfügbar
        editTextPrice = (EditText)findViewById(R.id.edit_input_Price);
        editTextBeschreibung = (EditText)findViewById(R.id.edit_input_beschreibung);
        editTextPlz=(EditText)findViewById(R.id.edit_input_plz);
        editTextStasse=(EditText)findViewById(R.id.edit_input_strasse);
        editTextStadt=(EditText)findViewById(R.id.edit_input_stadt);
        editTextPhone = (EditText) findViewById(R.id.edit_input_phone);
        editTextIdErsteller=null; //fixme me
        editTextListBesteller=null;
        dateErstellen=Calendar.getInstance().getTime();//new Date();//aktuelle Datum
        mImageView = (ImageView)findViewById(R.id.edit_input_image);


        editTextName.setText(getIntent().getStringExtra("objektName"));
       // spinnerKategorie.setText(getIntent().getStringExtra("objektKategorie"));
//        float price=Float.parseFloat(getIntent().getStringExtra("objektPrice"));
//        String priceString=String.valueOf(price);
//        //editTextPrice.setText(priceString);

        //detail_objekt_date.setText(getIntent().getStringExtra("objektDate"));
        editTextBeschreibung.setText(getIntent().getStringExtra("objektBeschreibung"));
        editTextPlz.setText(getIntent().getStringExtra("objektPlz"));
        editTextStasse.setText(getIntent().getStringExtra("objektStrasse"));
        editTextStadt.setText(getIntent().getStringExtra("objektStadt"));

        getSupportActionBar().setTitle("Edit :  " +getIntent().getStringExtra("objektName")); // title an page
        Picasso.get()
                //.load("https://parismatch.be/app/uploads/2018/04/Macaca_nigra_self-portrait_large-e1524567086123-1100x715.jpg")
                //.load(new File(objektCurrent.getImage()))
                .load(getIntent().getStringExtra("objektImage"))
                .placeholder(R.mipmap.ic_launcher_round)
                //.error(R.mipmap.add_input_image_muster)
                .noFade().resize(150,150)
                .centerCrop()
                .into(mImageView);



        btnAbbrechen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(),DetailMyObjektActivity.class);
                intent.putExtra("objektID",getIntent().getStringExtra("objektID"));
                intent.putExtra("objektImage", getIntent().getStringExtra("objektImage"));
                // intent_edit.putExtra("objektType",  mObjekts.get(position).getType);
                intent.putExtra("objektName", getIntent().getStringExtra("objektName"));
                intent.putExtra("objektKategorie", getIntent().getStringExtra("objektKategorie"));
                intent.putExtra("objektPrice", getIntent().getStringExtra("objektPrice"));
                intent.putExtra("objektDate",  getIntent().getStringExtra("objektDate"));
                intent.putExtra("objektBeschreibung",getIntent().getStringExtra("objektBeschreibung"));
                intent.putExtra("idErsteller",  getIntent().getStringExtra("idErsteller"));
                //intent_edit.putExtra("Phone", 123456  );
                // intent_edit.putExtra("objektAdresse",  mObjekts.get(position).getPlz().concat(mObjekts.get(position).getStrasse()));
                intent.putExtra("objektPlz", getIntent().getStringExtra("objektPlz"));
                intent.putExtra("objektStrasse", getIntent().getStringExtra("objektStrasse"));
                intent.putExtra("objektStadt", getIntent().getStringExtra("objektStadt"));
                intent.putExtra("objektDate",  getIntent().getStringExtra("objektDate"));
                startActivity(intent);

            }
        });


        btnSaveNew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NameHolder = editTextName.getText().toString().trim() ;
                KategorieHolder=spinnerKategorie.getSelectedItem().toString().trim();
                PriceHolder = editTextPrice.getText().toString().trim() ;
                float price= Float.parseFloat(PriceHolder);
                BeschreibungHolder = editTextBeschreibung.getText().toString().trim() ;
                selectedId_edit = radioGroup.getCheckedRadioButtonId();
                TypeHolder=true; //Todo Verfügbarkeit des Objekts
                int selectedId = radioGroup.getCheckedRadioButtonId();
                radioButtonType = (RadioButton) findViewById(selectedId);
                EtatHolder= String.valueOf(radioButtonType); // verloren, verleihen oder tauschen
                PlzHolder=editTextPlz.getText().toString().trim();
                StrasseHolder=editTextStasse.getText().toString().trim();
                StadtHolder=editTextStadt.getText().toString().trim();
                PhoneHolder = editTextPhone.getText().toString().trim() ;
//                //String PhoneNr= Long.getLong(PhoneHolder); //fixme or todo wenn crash
////                IdErstellerHolder="{\"email\":\""+email_user+"\"," +
////                        "           \"phone\":\""+PhoneHolder+"\"}"; //email_user
                IdErstellerHolder =getIntent().getStringExtra("idErsteller");
                ListIdBestellerHolder=null;
                dateErstellen=Calendar.getInstance().getTime();
                ImageAdresseHolder = getIntent().getStringExtra("objektImage"); //Fixme new Technick
               // Toast.makeText(EditMyObjektActivity.this," objektCurrentID ist:" +price,Toast.LENGTH_LONG).show();


                Objekt objektNew=new Objekt(
                        NameHolder,
                        KategorieHolder,
                        EtatHolder,
                        true,
                        price,
                        BeschreibungHolder,
                        PlzHolder,
                        StrasseHolder,
                        StadtHolder,
                        IdErstellerHolder,
                        ListIdBestellerHolder,
                        ImageAdresseHolder,
                        dateErstellen);

                String objektCurrentID = getIntent().getStringExtra("objektID");
                saveNewObjekt(objektCurrentID,objektNew);

            }
        });

    }
    public  void saveNewObjekt(String objektCurrentID, Objekt objektNew){
        DatabaseReference dR = FirebaseDatabase.getInstance().getReference("Objekt").child(objektCurrentID);
        dR.setValue(objektNew); // firebase Update  todo connection check

        Intent intent =  new Intent(getApplicationContext(),DetailMyObjektActivity.class);
        intent.putExtra("objektID",objektCurrentID);
        intent.putExtra("objektImage",  objektNew.getImage());
        // intent.putExtra("objektType",  mObjekts.get(position).getType); //  fixme  false or true
        intent.putExtra("objektName",  objektNew.getName());
        intent.putExtra("objektKategorie",  objektNew.getKategorie()).toString();
        intent.putExtra("objektPrice", ( objektNew.getPrice())).toString();
        intent.putExtra("objektDate",  objektNew.getDate_objekt_erstellen());
        intent.putExtra("objektBeschreibung", objektNew.getBeschreibung());
        intent.putExtra("idErsteller", objektNew.getId_ersteller());
        //intent.putExtra("Phone", 123456  );
        intent.putExtra("objektAdresse",  objektNew.getPlz().concat(objektNew.getStrasse()));
        intent.putExtra("objektPlz",  objektNew.getPlz());
        intent.putExtra("objektStrasse",  objektNew.getStrasse());
        intent.putExtra("objektStadt",  objektNew.getStadt());
        intent.putExtra("objektDate", objektNew.getDate_objekt_erstellen());

        Toast.makeText(EditMyObjektActivity.this," Updaten Sind gut gelaufen" ,Toast.LENGTH_LONG).show();
        startActivity(intent);


    }

    //convertor Date
    public static String getDateErstellen(Date date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "EEE, d MMM yyyy HH:mm", Locale.getDefault());
        return dateFormat.format(date);
    }
}
