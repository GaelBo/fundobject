package com.example.lawe.fundobject.adapters_helper;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class SQLiteHelper extends SQLiteOpenHelper {

    public static String DATABASE_NAME = "FundObjekt";
    public static final String TABLE_NAME = "ObjektTable";

    public static final String TABLE_NAME2 = "ObjektTable_Image";
    public static final String TABLE_NAME3 = "userTableV0";

    public static final String Table_Column_ID = "id";
    public static final String Table_Column_1_Name = "name";
    public static final String Table_Column_2_Kategorie = "kategorie";
    public static final String Table_Column_3_Price = "price";
    public static final String Table_Column_4_Beschreibung = "beschreibung";
    //ajout des champs
    public static final String Table_Column_5_Bild = "bild";
    public static final String Table_Column_6_Type="type"; //verloren oder Verleihen
    public static final String Table_Column_7_Etat="etat"; //verfügbar oder nicht
    public static final String Table_Column_8_IdErsteller="id_ersteller";
    public static final String Table_Column_9_ListIdBesteller="list_id_besteller";
    public static final String Table_Column_10_Plz="plz_objekt";
    public static final String Table_Column_11_Strasse="strasse_objekt";
    public static final String Table_Column_12_Stadt="stadt_objekt";
    public static final String Table_Column_13_Date="date_erstellen";



    private String CREATE_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + " (" + Table_Column_ID + " INTEGER PRIMARY KEY, " + Table_Column_1_Name + " VARCHAR, " + Table_Column_2_Kategorie + " VARCHAR, " + Table_Column_3_Price + " VARCHAR," + Table_Column_4_Beschreibung + " VARCHAR)";
    private String CREATE_TABLE2 = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME2 +
            " (" + Table_Column_ID + " INTEGER PRIMARY KEY, "
            + Table_Column_1_Name + " VARCHAR, "
            + Table_Column_2_Kategorie + " VARCHAR, "
            + Table_Column_3_Price + " VARCHAR,"
            + Table_Column_4_Beschreibung + " VARCHAR,"

            + Table_Column_5_Bild + " BLOD,"
            + Table_Column_6_Type + " BOOL,"
            + Table_Column_7_Etat + " BOOL,"
            + Table_Column_8_IdErsteller + " VARCHAR,"
            + Table_Column_9_ListIdBesteller + " VARCHAR,"
            + Table_Column_11_Strasse + " VARCHAR,"
            + Table_Column_12_Stadt + " VARCHAR,"
            + Table_Column_13_Date + " DATE)";


    public SQLiteHelper(Context context) {
        super(context, DATABASE_NAME, null, 1);

    }

    @Override
    public void onCreate(SQLiteDatabase database) {


        database.execSQL(CREATE_TABLE);
        database.execSQL(CREATE_TABLE2);


    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME2);
        onCreate(db);

    }


}