package com.example.lawe.fundobject.adapters_helper;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.lawe.fundobject.R;

import java.util.ArrayList;

public class MyListAdapter extends BaseAdapter {

    Context context;
    ArrayList<String> ID;
    ArrayList<String> Name;
    ArrayList<String> Kategorie;
    ArrayList<String> Price;
    ArrayList<String> Beschreibung;


    public MyListAdapter(Context context2,
                         ArrayList<String> id,
                         ArrayList<String> name,
                         ArrayList<String> kategorie,
                         ArrayList<String> price,
                         ArrayList<String> beschreibung
    )
    {

        this.context = context2;
        this.ID = id;
        this.Name = name;
        this.Kategorie = kategorie;
        this.Price=price;
        this.Beschreibung=beschreibung;

    }

    public int getCount() {
        // TODO Auto-generated method stub
        return ID.size();
    }

    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return null;
    }

    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return 0;
    }

    public View getView(int position, View child, ViewGroup parent) {

        Holder holder;

        LayoutInflater layoutInflater;

        if (child == null) {
            layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            child = layoutInflater.inflate(R.layout.adapter_objekt_item, null);

            holder = new Holder();

            holder.ID_TextView = (TextView) child.findViewById(R.id.item_objekt_date);
            holder.Name_TextView = (TextView) child.findViewById(R.id.item_objekt_name);
            holder.Kategorie_TextView = (TextView) child.findViewById(R.id.item_objekt_name);
            holder.Price_TextView = (TextView) child.findViewById(R.id.item_objekt_price);
            holder.Beschreibung_TextView = (TextView) child.findViewById(R.id.item_objekt_price);

            child.setTag(holder);

        } else {

            holder = (Holder) child.getTag();
        }
        holder.ID_TextView.setText(ID.get(position));
        holder.Name_TextView.setText(Name.get(position));
        holder.Kategorie_TextView.setText(Kategorie.get(position));
        holder.Price_TextView.setText(Price.get(position));
        holder.Beschreibung_TextView.setText(Beschreibung.get(position));


        return child;
    }

    public class Holder {

        TextView ID_TextView;
        TextView Name_TextView;
        TextView Kategorie_TextView;
        TextView Price_TextView;
        TextView Beschreibung_TextView;
    }

}