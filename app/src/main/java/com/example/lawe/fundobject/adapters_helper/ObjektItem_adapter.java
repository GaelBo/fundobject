package com.example.lawe.fundobject.adapters_helper;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.lawe.fundobject.R;
import com.example.lawe.fundobject.create_object.ObjektDetailActivity;
import com.example.lawe.fundobject.models.Objekt;

import java.util.List;

public class ObjektItem_adapter extends BaseAdapter {

    /*on passe tout le context a l adapter: les activites, fragment...*/
    private Context context;
    private List<Objekt>objektItemList;
    //pour personnaliser nos objekt a l affichage
    private LayoutInflater inflater;

    //constr
    public ObjektItem_adapter(Context c,List<Objekt> objektItemList){
        this.context=c;
        this.objektItemList=objektItemList;
        this.inflater=LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return objektItemList.size();
    }

    @Override
    public Objekt getItem(int i) {
        return objektItemList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        //personaliser chaque elt avec in Inflater,et
        //l injecter autant que necessaire avec LayoutInflater
        view =inflater.inflate(R.layout.adapter_objekt_item,null);

        //get info about item
        Objekt currentItem=getItem(i); //fixme OjektItem
        final String itemName=currentItem.getName();
        String itemKategoriee=currentItem.getKategorie();
        double itemPrice=currentItem.getPrice();
        String itemBeschreibung=currentItem.getBeschreibung();
        //par default a une categorie, j associe une foto, donc itemMemonic fait ref au nom d la tof
        String itemMemonic=currentItem.getKategorie(); //utiliser pour trouver sa ressource correspondante

        //get item infos view
        TextView itemNameView=view.findViewById(R.id.item_objekt_name);
        TextView itemAdresseView=view.findViewById(R.id.item_objekt_name);
        TextView itemPriceView=view.findViewById(R.id.item_objekt_price);
        TextView itemBeschreibungView=view.findViewById(R.id.item_objekt_name);
        //get special image with:
        ImageView itemImageMemonicView=view.findViewById(R.id.item_objekt_image);
        //recuperer la ressourse imag a partir de son name, TAG, memonic

        int resImageId=context.getResources().getIdentifier(itemMemonic,"drawable",context.getPackageName());

        itemNameView.setText(itemName);
        itemAdresseView.setText(itemKategoriee);
        itemPriceView.setText(itemPrice+" € ");
        itemAdresseView.setText(itemBeschreibung);
        itemImageMemonicView.setImageResource(resImageId);

        //wenn es geklickt wurde
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               // Toast.makeText(context,"Sie wollen"+itemName+ "detail sehen ? ahahha",Toast.LENGTH_LONG).show();

                Intent myIntent = new Intent(context, ObjektDetailActivity.class);
                myIntent.putExtra("key", itemName); //Optional parameters
                context.startActivity(myIntent);

            }
        });
        return view;
    }

}
