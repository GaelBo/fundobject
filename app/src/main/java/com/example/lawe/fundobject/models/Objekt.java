package com.example.lawe.fundobject.models;

import android.support.annotation.Nullable;

import java.util.Date;

public class Objekt {


   // private long id_objekt;
    @Nullable private String name;
    private String kategorie;
    private String type; //verloren , tauschen oder verleihen
    private  boolean zustand ; //true: verfügbar oder false= nicht
    private float price;
    private String beschreibung;
    private String plz;
    private String strasse;
    private String stadt;
    private String id_ersteller; // @Param email/Phone Ersteller
    @Nullable private String list_id_besteller ;  //@Param LIST emailBesteller,
    private String Uriimage;
    private Date date_objekt_erstellen;

    //constructor
    public Objekt(String name, String kategorie, String type, boolean zustand, float price, String beschreibung, String plz, String strasse, String stadt, String id_ersteller, String list_id_besteller,String image, Date date_objekt_erstellen) {
       // this.id_objekt = id_objekt;
        this.name = name;
        this.kategorie = kategorie;
        this.type = type;
        this.zustand = zustand;
        this.price = price;
        this.beschreibung = beschreibung;
        this.plz = plz;
        this.strasse = strasse;
        this.stadt = stadt;
        this.id_ersteller = id_ersteller;
        this.list_id_besteller = list_id_besteller;
        this.Uriimage = image;
        this.date_objekt_erstellen = date_objekt_erstellen;
    }

    //constructeur
    public Objekt(){
        //empty constructor needed
        this.name = "FundObject";
        this.kategorie = "Ausweise";
        this.type = "tauschen";
        this.zustand = true;
        this.price = 80000;
        this.beschreibung = "Die App hilf uns eine Objekt zu finden";
        this.plz = "78945";
        this.strasse = "Str 5";
        this.stadt = "Worms";
        this.id_ersteller = "Gael/Cesaire";
        this.list_id_besteller = null;
        this.Uriimage = null;
        this.date_objekt_erstellen = null;
    }

    //getts un setts
//    public long getId_objekt() {
//        return id_objekt;
//    }
//
//    public void setId_objekt(long id_objekt) {
//        this.id_objekt = id_objekt;
//    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getKategorie() {
        return kategorie;
    }

    public void setKategorie(String kategorie) {
        this.kategorie = kategorie;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean isZustand() {
        return zustand;
    }

    public void setZustand(boolean zustand) {
        this.zustand = zustand;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public String getBeschreibung() {
        return beschreibung;
    }

    public void setBeschreibung(String beschreibung) {
        this.beschreibung = beschreibung;
    }

    public String getPlz() {
        return plz;
    }

    public void setPlz(String plz) {
        this.plz = plz;
    }

    public String getStrasse() {
        return strasse;
    }

    public void setStrasse(String strasse) {
        this.strasse = strasse;
    }

    public String getStadt() {
        return stadt;
    }

    public void setStadt(String stadt) {
        this.stadt = stadt;
    }

    public String getId_ersteller() {
        return id_ersteller;
    }

    public void setId_ersteller(String id_ersteller) {
        this.id_ersteller = id_ersteller;
    }

    public String getList_id_besteller() {
        return list_id_besteller;
    }

    public void setList_id_besteller(String list_id_besteller) {
        this.list_id_besteller = list_id_besteller;
    }

    public String getImage() {
        return Uriimage;
    }

    public void setImage(String image) {
        this.Uriimage = image;
    }

    public Date getDate_objekt_erstellen() {
        return date_objekt_erstellen;
    }

    public void setDate_objekt_erstellen(Date date_objekt_erstellen) {
        this.date_objekt_erstellen = date_objekt_erstellen;
    }

}