package com.example.lawe.fundobject.notifications;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.support.annotation.NonNull;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;
import android.widget.Toast;

import com.example.lawe.fundobject.MainActivity;
import com.example.lawe.fundobject.R;
import com.example.lawe.fundobject.adapters_helper.DetailsActivity;
import com.example.lawe.fundobject.create_object.HomeActivity;
import com.example.lawe.fundobject.home_user.HomePageUser;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import static com.example.lawe.fundobject.home_user.Fragment_home_page_user_Nachricht.TAG;


public class NotificationsHelper extends FirebaseMessagingService {

  private static FirebaseAuth mAuth;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        if (remoteMessage.getNotification() != null) {
            String title = remoteMessage.getNotification().getTitle();
            String body = remoteMessage.getNotification().getBody();
            String token ="ex9Ytii5KbI:APA91bHaWZy27CTBYd9jbFQWICXMMXetcsF1oKsI8f8_vs8J5ktIzTqM80IEzsEmOZCiDK3w3RkoXu3f8N1rlziSYZEL0O-ddxMaKWPBrCeQStcv9ZiqEHttMuILenCBPv94mLgDuVac";


          //  NotificationsHelper.displayNotification(getApplicationContext(),title,body, token);
        }
    }

    //my personliche Funktion für Notification
    public static void displayNotification(Context context, String title, String body, String token){

        // to start new activity bei Notification, user particular Intent
        // because hir coming another activitiy and foreign
        Intent intent = new Intent(context, HomePageUser.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(context,
                100,
                intent,
                PendingIntent.FLAG_CANCEL_CURRENT
        );

                // 2 - Create a Style for the Notification
        NotificationCompat.InboxStyle inboxStyle = new NotificationCompat.InboxStyle();
        inboxStyle.setBigContentTitle(title);
        inboxStyle.addLine(body);

        //  - Build a Notification object
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(context, MainActivity.CHANNEL_ID)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle(title)
                        .setContentText(body)
                        .setContentIntent(pendingIntent) //action to notification class intent
                        .setAutoCancel(true)
                        .setStyle(inboxStyle)
                        .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));


        //Notification Manager to set Phone view
        NotificationManagerCompat notificationManagerCompat = NotificationManagerCompat.from(context);
        notificationManagerCompat.notify(1,mBuilder.build());


    }

    public static void sendNotificationSubscribe(Context context, String title, String body, String token){

        mAuth = FirebaseAuth.getInstance();
        FirebaseMessaging.getInstance().subscribeToTopic("updates");
        // to start new activity bei Notification, user particular Intent
        // because hir coming another activitiy and foreign
        Intent intent = new Intent(context, HomePageUser.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(context,
                100,
                intent,
                PendingIntent.FLAG_CANCEL_CURRENT
        );

        // 2 - Create a Style for the Notification
        NotificationCompat.InboxStyle inboxStyle = new NotificationCompat.InboxStyle();
        inboxStyle.setBigContentTitle(title);
        inboxStyle.addLine(body);

        //  - Build a Notification object
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(context, MainActivity.CHANNEL_ID)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle(title)
                        .setContentText(body)
                        .setContentIntent(pendingIntent) //action to notification class intent
                        .setAutoCancel(true)
                        .setStyle(inboxStyle)
                        .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));


        //Notification Manager to set Phone view
        NotificationManagerCompat notificationManagerCompat = NotificationManagerCompat.from(context);
        notificationManagerCompat.notify(1,mBuilder.build());


    }

}
