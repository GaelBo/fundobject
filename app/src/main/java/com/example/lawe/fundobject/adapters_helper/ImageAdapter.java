package com.example.lawe.fundobject.adapters_helper;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.lawe.fundobject.R;
import com.example.lawe.fundobject.create_object.HomeActivity;
import com.example.lawe.fundobject.create_object.ObjektDetailActivity;
import com.example.lawe.fundobject.models.Objekt;
import com.squareup.picasso.Picasso;


import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import uk.co.senab.photoview.PhotoViewAttacher;

import static android.text.TextUtils.concat;

public class ImageAdapter extends RecyclerView.Adapter<ImageAdapter.ImageViewHolder> {
    private Context mContext;
    private List<Objekt> mObjekts;
    private OnItemClickListener mListener;

    //need constructeur with content und list
    public ImageAdapter(Context context, List<Objekt>objekts){
        mContext = context;
        mObjekts = objekts;
    }


    @Override
    public ImageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(mContext).inflate(R.layout.adapter_objekt_item,parent,false);
        return new ImageViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ImageViewHolder holder, int position) {
        Objekt objektCurrent = mObjekts.get(position);
        holder.textViewDate.setText(getDateErstellen(objektCurrent.getDate_objekt_erstellen()));
        holder.textViewName.setText(objektCurrent.getName());
        holder.textViewPrice.setText(concat(String.valueOf(objektCurrent.getPrice()), " €- " ));
        holder.textViewAdresse.setText(concat(objektCurrent.getStrasse(), " - " , objektCurrent.getPlz(), " - ",objektCurrent.getStadt()));
        holder.textViewType.setText(objektCurrent.getType());
        //true=verloren false=verleihen
        //if (objektCurrent.isType()==false) holder.textViewType.setText("Verloren");else holder.textViewType.setText("Verleihen");
        Picasso.get()
                .load(objektCurrent.getImage())
                .placeholder(R.mipmap.ic_launcher_round)
                .error(R.mipmap.ic_launcher_round)
                .noFade().resize(150,150)
                .centerCrop()
                .into(holder.imageView);
    }

    @Override
    public int getItemCount() {
        return mObjekts.size();
    }

    public class ImageViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView textViewName,textViewAdresse,textViewDate, textViewType, textViewPrice;
        public ImageView imageView;

        //constructor
        public ImageViewHolder(View itemView){
            super(itemView);
            textViewName=itemView.findViewById(R.id.item_objekt_name);
            textViewDate = itemView.findViewById(R.id.item_objekt_date);
            textViewPrice = itemView.findViewById(R.id.item_objekt_price);
            textViewType = itemView.findViewById(R.id.item_objekt_type);
            textViewAdresse = itemView.findViewById(R.id.item_objekt_strasse);
            imageView = itemView.findViewById(R.id.item_objekt_image);

            itemView.setOnClickListener(this);


        }

        @Override
        public void onClick(View v) {
            if(mListener !=null){
                int position = getAdapterPosition();
                if(position != RecyclerView.NO_POSITION){
                    mListener.onItemClick(position);
                }
            }
        }
    } //end unter class
    //convertor Date
    public static String getDateErstellen(Date date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "EEE, d MMM yyyy HH:mm", Locale.getDefault());
        return dateFormat.format(date);
    }

    //creer les interfaces (fonction) appeler lors du clic sur un elt de la liste
    public interface OnItemClickListener {

        void onItemClick(int position);
    }
    //utilisons cett interface ici en param
    public void setOnItemClickListener(OnItemClickListener listener){
        mListener = listener;
    }

}

//    // Class variables for the Cursor that holds task data and the Context
//    private Cursor mCursor;
//    private Context mContext;
//
//
//    /**
//     * Constructor for the CustomCursorAdapter that initializes the Context.
//     *
//     * @param mContext the current Context
//     */
//    public ImagesAdapter(Context mContext) {
//        this.mContext = mContext;
//    }
//
//
//
//    @Override
//    public ImagesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//        LayoutInflater inflater = LayoutInflater.from(mContext);
//        View view = inflater.inflate(R.layout.image_item, parent, false);
//        return new ImagesViewHolder(view);
//    }
//
//    @Override
//    public void onBindViewHolder(ImagesViewHolder holder, int position) {
//
//        // Indices for the _id, description, and priority columns
//        //int idIndex = mCursor.getColumnIndex(_ID);
//        int fragranceName = mCursor.getColumnIndex(DbHelper.COLUMN_NAME);
//
//
//
//        mCursor.moveToPosition(position); // get to the right location in the cursor
//
//        // Determine the values of the wanted data
//       // final int id = mCursor.getInt(idIndex);
//        byte[] image = mCursor.getBlob(fragranceName);
//
//        //Set values
//       // holder.itemView.setTag(id);
//
//        Bitmap bmp = BitmapFactory.decodeByteArray(image, 0, image.length);
//        holder.image.setImageBitmap(Bitmap.createScaledBitmap(bmp, 200,
//                200, false));
//
//
//    }
//
//
//    @Override
//    public int getItemCount() {
//        if (mCursor == null) {
//            return 0;
//        }
//        return mCursor.getCount();
//    }
//
//    public Cursor swapCursor(Cursor c) {
//        // check if this cursor is the same as the previous cursor (mCursor)
//        if (mCursor == c) {
//            return null; // bc nothing has changed
//        }
//        Cursor temp = mCursor;
//        this.mCursor = c; // new cursor value assigned
//
//        //check if this is a valid cursor, then update the cursor
//        if (c != null) {
//            this.notifyDataSetChanged();
//        }
//        return temp;
//    }
//
//    public class ImagesViewHolder extends RecyclerView.ViewHolder {
//        ImageView image;
//        public ImagesViewHolder(View itemView) {
//            super(itemView);
//
//            image = (ImageView) itemView.findViewById(R.id.imageProf);
//        }
//
//    }



//package com.example.lawe.fundobject.adapters_helper;
//
//import android.content.Context;
//import android.content.Intent;
//import android.database.Cursor;
//import android.graphics.Bitmap;
//import android.graphics.BitmapFactory;
//
//import android.net.Uri;
//import android.support.v7.widget.RecyclerView;
//import android.util.Log;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.ImageView;
//import android.widget.TextView;
//import android.widget.Toast;
//
//import com.example.lawe.fundobject.R;
//import com.example.lawe.fundobject.create_object.HomeActivity;
//import com.example.lawe.fundobject.create_object.ObjektDetailActivity;
//import com.example.lawe.fundobject.models.Objekt;
//import com.squareup.picasso.Picasso;
//
//
//import java.io.File;
//import java.text.SimpleDateFormat;
//import java.util.Date;
//import java.util.List;
//import java.util.Locale;
//
//import static android.text.TextUtils.concat;
//
//public class ImageAdapter extends RecyclerView.Adapter<ImageAdapter.ImageViewHolder> {
//    private Context mContext;
//    private List<Objekt> mObjekts;
//    private OnItemClickListener mListener;
//
//    //need constructeur with content und list
//    public ImageAdapter(Context context, List<Objekt>objekts){
//        mContext = context;
//        mObjekts = objekts;
//    }
//
//
//    @Override
//    public ImageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//        View v = LayoutInflater.from(mContext).inflate(R.layout.adapter_objekt_item,parent,false);
//        return new ImageViewHolder(v);
//    }
//
//    @Override
//    public void onBindViewHolder(ImageViewHolder holder, int position) {
//        Objekt objektCurrent = mObjekts.get(position);
//        holder.textViewDate.setText(getDateErstellen(objektCurrent.getDate_objekt_erstellen()));
//        holder.textViewName.setText(objektCurrent.getName());
//        holder.textViewPrice.setText(concat(String.valueOf(objektCurrent.getPrice()), " €- " ));
//        holder.textViewAdresse.setText(concat(objektCurrent.getStrasse(), " - " , objektCurrent.getPlz()));
//        //true=verloren false=verleihen
//        if (objektCurrent.isType()==false) holder.textViewType.setText("Verloren");else holder.textViewType.setText("Verleihen");
//
//
//        //essai  //http://i.imgur.com/DvpvklR.png
//        Picasso.with(mContext)
//                //.load("https://parismatch.be/app/uploads/2018/04/Macaca_nigra_self-portrait_large-e1524567086123-1100x715.jpg")
//                //.load(new File(objektCurrent.getImage()))
//               // .load("https://firebasestorage.googleapis.com/v0/b/fundobjekt.appspot.com/o/Objekt%2F1546896328952.jpg")
//                .load(objektCurrent.getImage())
//                .placeholder(R.mipmap.add_input_image_muster)
//                .error(R.drawable.handy1)
//                .noFade().resize(150,150)
//                .centerCrop()
//                .into(holder.imageView);
//        }
//
//    @Override
//    public int getItemCount() {
//        return mObjekts.size();
//    }
//
//    public class ImageViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
//        public TextView textViewName,textViewAdresse,textViewDate, textViewType, textViewPrice;
//        public ImageView imageView;
//
//        //constructor
//        public ImageViewHolder(View itemView){
//            super(itemView);
//            textViewName=itemView.findViewById(R.id.item_objekt_name);
//            textViewDate = itemView.findViewById(R.id.item_objekt_date);
//            textViewPrice = itemView.findViewById(R.id.item_objekt_price);
//            textViewType = itemView.findViewById(R.id.item_objekt_type);
//            textViewAdresse = itemView.findViewById(R.id.item_objekt_strasse);
//            imageView = itemView.findViewById(R.id.item_objekt_image);
//
//            itemView.setOnClickListener(this);
//
//
//        }
//
//        @Override
//        public void onClick(View v) {
//            if(mListener !=null){
//                int position = getAdapterPosition();
//                if(position != RecyclerView.NO_POSITION){
//                   mListener.onItemClick(position);
//                           }
//            }
//        }
//    } //end unter class
//    //convertor Date
//    public static String getDateErstellen(Date date) {
//        SimpleDateFormat dateFormat = new SimpleDateFormat(
//                "EEE, d MMM yyyy HH:mm", Locale.getDefault());
//        return dateFormat.format(date);
//    }
//
//    //creer les interfaces (fonction) appeler lors du clic sur un elt de la liste
//    public interface OnItemClickListener {
//
//        void onItemClick(int position);
//    }
//    //utilisons cett interface ici en param
//    public void setOnItemClickListener(OnItemClickListener listener){
//        mListener = listener;
//    }
//
//}
//
////    // Class variables for the Cursor that holds task data and the Context
////    private Cursor mCursor;
////    private Context mContext;
////
////
////    /**
////     * Constructor for the CustomCursorAdapter that initializes the Context.
////     *
////     * @param mContext the current Context
////     */
////    public ImagesAdapter(Context mContext) {
////        this.mContext = mContext;
////    }
////
////
////
////    @Override
////    public ImagesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
////        LayoutInflater inflater = LayoutInflater.from(mContext);
////        View view = inflater.inflate(R.layout.image_item, parent, false);
////        return new ImagesViewHolder(view);
////    }
////
////    @Override
////    public void onBindViewHolder(ImagesViewHolder holder, int position) {
////
////        // Indices for the _id, description, and priority columns
////        //int idIndex = mCursor.getColumnIndex(_ID);
////        int fragranceName = mCursor.getColumnIndex(DbHelper.COLUMN_NAME);
////
////
////
////        mCursor.moveToPosition(position); // get to the right location in the cursor
////
////        // Determine the values of the wanted data
////       // final int id = mCursor.getInt(idIndex);
////        byte[] image = mCursor.getBlob(fragranceName);
////
////        //Set values
////       // holder.itemView.setTag(id);
////
////        Bitmap bmp = BitmapFactory.decodeByteArray(image, 0, image.length);
////        holder.image.setImageBitmap(Bitmap.createScaledBitmap(bmp, 200,
////                200, false));
////
////
////    }
////
////
////    @Override
////    public int getItemCount() {
////        if (mCursor == null) {
////            return 0;
////        }
////        return mCursor.getCount();
////    }
////
////    public Cursor swapCursor(Cursor c) {
////        // check if this cursor is the same as the previous cursor (mCursor)
////        if (mCursor == c) {
////            return null; // bc nothing has changed
////        }
////        Cursor temp = mCursor;
////        this.mCursor = c; // new cursor value assigned
////
////        //check if this is a valid cursor, then update the cursor
////        if (c != null) {
////            this.notifyDataSetChanged();
////        }
////        return temp;
////    }
////
////    public class ImagesViewHolder extends RecyclerView.ViewHolder {
////        ImageView image;
////        public ImagesViewHolder(View itemView) {
////            super(itemView);
////
////            image = (ImageView) itemView.findViewById(R.id.imageProf);
////        }
////
////    }
//
//
