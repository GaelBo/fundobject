package com.example.lawe.fundobject.adapters_helper;

import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.lawe.fundobject.R;

public class DetailsActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor> {

    ImageAdapter imagesAdapter;
    RecyclerView mRecyclerView;

    private static final int IMAGES_LOADER = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
//
//        // Set the RecyclerView to its corresponding view
//        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);
//
//        // Set the layout for the RecyclerView to be a linear layout, which measures and
//        // positions items within a RecyclerView into a linear list
//        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
//
//        // Initialize the adapter and attach it to the RecyclerView
//        imageAdapter = new ImageAdapter(this);
//        mRecyclerView.setAdapter(imageAdapter);
//
//        getLoaderManager().initLoader(IMAGES_LOADER, null, this);


    }
    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        // Define a projection that specifies the columns from the table we care about.
        String[] projection = {
                DbHelper.COLUMN_NAME,
        };

        // This loader will execute the ContentProvider's query method on a background thread
        return new CursorLoader(this,   // Parent activity context
                ImagesProvider.CONTENT_URI,   // Provider content URI to query
                projection,             // Columns to include in the resulting Cursor
                null,                   // No selection clause
                null,                   // No selection arguments
                null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {

        //imageAdapter.swapCursor(cursor);

    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

       // imagesAdapter.swapCursor(null);

    }
}
