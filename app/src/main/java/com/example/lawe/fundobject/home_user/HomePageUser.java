package com.example.lawe.fundobject.home_user;

import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import android.widget.Button;
import android.widget.Toast;

import com.example.lawe.fundobject.MainActivity;
import com.example.lawe.fundobject.R;
import com.example.lawe.fundobject.create_object.HomeActivity;
import com.example.lawe.fundobject.create_object.ObjektErstellenActivity;
import com.example.lawe.fundobject.konfiguration.Notification;
import com.example.lawe.fundobject.konfiguration.Rechtline;
import com.example.lawe.fundobject.registrierung_anmeldung.SessionUser;

public class HomePageUser extends AppCompatActivity {


    private SectionsPagerAdapter mSectionsPagerAdapter;

    private ViewPager mViewPager;
    private Button speicher;
    SessionUser sessionUser;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_page_user);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);

        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPager));



    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_home_user, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id= item.getItemId();
        if (id==R.id.action_notifications){
            Intent intent= new Intent(HomePageUser.this,Notification.class);
            startActivity(intent);
        }
        else
        if (id==R.id.action_home_page){

            Intent intent= new Intent(HomePageUser.this,HomeActivity.class);

            String name_user= getIntent().getStringExtra("name");
            String email_user= getIntent().getStringExtra("email");
            int id_user=getIntent().getIntExtra("id_user",0);

            intent.putExtra("id_user",id_user);
            intent.putExtra("name",name_user);
            intent.putExtra("email",email_user);

            startActivity(intent);
        }
        else
        if (id==R.id.action_objekt_ertellen){
            Intent intent= new Intent(HomePageUser.this,ObjektErstellenActivity.class);

            String name_user= getIntent().getStringExtra("name");
            String email_user= getIntent().getStringExtra("email");
            int id_user=getIntent().getIntExtra("id_user",0);

            intent.putExtra("id_user",id_user);
            intent.putExtra("name",name_user);
            intent.putExtra("email",email_user);

            startActivity(intent);
        }
        else
        if (id==R.id.action_logout){

            sessionUser = new SessionUser(getApplicationContext());
            sessionUser.logoutUser();

            Intent intent= new Intent(HomePageUser.this,HomeActivity.class);
            intent.putExtra("sessionUser","disconnect");
            startActivity(intent);
        }
        else
        if (id==R.id.action_rechtlicheHinweise){
            Intent intent= new Intent(HomePageUser.this,Rechtline.class);
            startActivity(intent);
        }
        /*
        else
        if (id==R.id.menu){
            Intent intent= new Intent(HomePageUser.this,MainActivity.class);
            startActivity(intent);
        } */
        else
        if (id==R.id.action_logout){
            Intent intent= new Intent(HomePageUser.this,HomeActivity.class);

            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }


    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }


        @Override
        public Fragment getItem(int position) {
            //Fragment fragment=null;

            switch (position){
                case 0:{
                    Fragment_home_page_user_publikation fragment= new Fragment_home_page_user_publikation();
                    Bundle bundle = new Bundle();
                   String email_user= getIntent().getStringExtra("email");
                  //  email_user= "cesaire";
                    bundle.putString("email",email_user);
                    fragment.setArguments(bundle);
                    return fragment;
                    // break;
                }
                case 1:{
                    //fragment = new RegistrierungFragment();
                    //break;
                    return new Fragment_home_page_user_Bestellung();
                }
                case 2:{
                    //fragment = new RegistrierungFragment();
                    //break;
                    return new Fragment_home_page_user_Nachricht();
                }
                default:
                    return null;

            }
            //return fragment;
        }

        @Override
        public int getCount() {

            return 3;
        }
    }





}
