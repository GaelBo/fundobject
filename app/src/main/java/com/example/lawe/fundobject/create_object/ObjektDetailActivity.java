package com.example.lawe.fundobject.create_object;

import android.Manifest;
import android.animation.Animator;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.lawe.fundobject.MainActivity;
import com.example.lawe.fundobject.R;
import com.example.lawe.fundobject.adapters_helper.SQLiteHelper;
import com.example.lawe.fundobject.home_user.HomePageUser;
import com.example.lawe.fundobject.models.Objekt;
import com.example.lawe.fundobject.models.User;
import com.example.lawe.fundobject.notifications.NotificationsHelper;
import com.example.lawe.fundobject.notifications.NotificationsService;
import com.example.lawe.fundobject.notifications.SendNotificationActivity;
import com.example.lawe.fundobject.registrierung_anmeldung.SessionUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import uk.co.senab.photoview.PhotoViewAttacher;

import static android.text.TextUtils.concat;
import static com.facebook.FacebookSdk.getApplicationContext;

public class ObjektDetailActivity extends AppCompatActivity {

    //für call to identifier permission request
    private static final int REQUEST_CALL =1 ;


    String IDholder;
    TextView id, name,kategorie, price,beschreibung,detail_objekt_Adresse,detail_objekt_date;
    ImageView image_objekt_detail;
    SQLiteDatabase sqLiteDatabase;
    SQLiteHelper sqLiteHelper;
    Cursor cursor;
    Button btnAbbrechen, btnCall, btnBestellen;
    String SQLiteDataBaseQueryHolder,id_objekt_bestellt,detail_objekt_id,userId_firebase="" ;
    String phone =null;
    Integer phone_call;
    // ScrollView scrollView;
    private   String email_user;
    SessionUser sessionUser;
    //persistence
    SQLiteDatabase sqLiteDatabaseObj;

    private Objekt mObjekt;
    private User user_save,user_recupere_firebase=null;
    private DatabaseReference mDatabaseRef,mDatabaseRef_user;
    private List<User> listeUser ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_objekt_detail);

        // id = (TextView) findViewById(R.id.detail_objekt_name);//fixme objekt_id_update
        name = (TextView) findViewById(R.id.detail_objekt_name);
        kategorie = (TextView) findViewById(R.id.detail_objekt_kategorie);
        price = (TextView) findViewById(R.id.detail_objekt_price);
        detail_objekt_date = (TextView) findViewById(R.id.detail_objekt_date);
        beschreibung = (TextView) findViewById(R.id.detail_objekt_beschreibung);
        detail_objekt_Adresse = (TextView)findViewById(R.id.detail_objekt_Adresse);
        image_objekt_detail = (ImageView) findViewById(R.id.image_objekt_detail);

        btnCall = (Button) findViewById(R.id.btnDetailCall);
        btnBestellen = (Button) findViewById(R.id.btnDetailBestellen);
        btnAbbrechen = (Button)findViewById(R.id.btnDetailAbbrechen);

//        sqLiteHelper = new SQLiteHelper(this);
        mDatabaseRef = FirebaseDatabase.getInstance().getReference("Objekt");
        //scrollView.setVisibility(View.VISIBLE);
        // mObjekt = (Objekt) getIntent().getSerializableExtra("objektDetail");
        // getIntent().getStringExtra("objektName");

        // id.setText(getIntent().getStringExtra("objektPrice")); //fixme float , int  Price todo
        name.setText(getIntent().getStringExtra("objektName"));
        kategorie.setText(getIntent().getStringExtra("objektKategorie"));
        price.setText(getIntent().getStringExtra("objektPrice"));

       detail_objekt_id=getIntent().getStringExtra("objektID");
        String dateString = getIntent().getStringExtra("objektDate");
       // Date dt = new Date(getIntent().getStringExtra("objektDate"));
        // detail_objekt_date.setText(dateString); //todo date fixed


        beschreibung.setText(getIntent().getStringExtra("objektBeschreibung"));
        detail_objekt_Adresse.setText(getIntent().getStringExtra("objektAdresse"));
        phone =getIntent().getStringExtra("Phone");

        getSupportActionBar().setTitle("Detail :  " +getIntent().getStringExtra("objektName")); // title an page
        Picasso.get()
                //.load("https://parismatch.be/app/uploads/2018/04/Macaca_nigra_self-portrait_large-e1524567086123-1100x715.jpg")
                //.load(new File(objektCurrent.getImage()))
                .load(getIntent().getStringExtra("objektImage"))
                .placeholder(R.drawable.handy1)
                //.error(R.mipmap.add_input_image_muster)
                .noFade().resize(150,150)
                .centerCrop()
                .into(image_objekt_detail);
        // photoViewAttacher = new PhotoViewAttacher(image_objekt_detail); local zoom

        image_objekt_detail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //function task 2 param, nameObjekt and PictureURI
                pictureZoom(getIntent().getStringExtra("objektName"),getIntent().getStringExtra("objektImage"));
            }
        });

        btnCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                makePhoneCall();
            }
        });
            //############################################## Bestellung speichern############################################################################
        btnBestellen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sessionUser = new SessionUser(getApplicationContext());
                if(sessionUser.isLoggedIn()) {
                    sessionUser.checkLogin();
                    // get user data from session
                    HashMap<String, String> user = sessionUser.getUserDetails();
                    email_user = user.get(SessionUser.KEY_EMAIL);
                    id_objekt_bestellt=detail_objekt_id;
                    String name_id_firebase = user.get(SessionUser.KEY_NAME);
                    try {
                        JSONObject info_user=new JSONObject(name_id_firebase);
                           userId_firebase=info_user.getString("userId_firebase");
                        name_id_firebase=info_user.getString("name");

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    user_save=new User();
                    user_save.setUserId_firebase(userId_firebase);
                    user_save.setName(name_id_firebase);
                    user_save.setEmail(email_user);


                    boolean resultat= save_bestellung(user_save,userId_firebase,id_objekt_bestellt);

                    if (resultat==true){

                        //Toast.makeText(getApplicationContext(), " Sie haben erstellt",Toast.LENGTH_LONG).show();

                        Intent intent = new Intent(getApplicationContext(),HomePageUser.class);
                        // startActivity(intent);
                    }else{
                        Toast.makeText(getApplicationContext(),"you have error ",Toast.LENGTH_LONG).show();
                    }
                    /*
                    DatabaseReference dR = FirebaseDatabase.getInstance().getReference("user").child(objektCurrentID);
                    dR.setValue(objektNew);
                    */
                    Toast.makeText(getApplicationContext(),email_user+" haben "+id_objekt_bestellt+"bestellen"+userId_firebase,Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(getApplicationContext(),HomePageUser.class);
                   // startActivity(intent);
                    //Notification Call
                    String title = getIntent().getStringExtra("objektKategorie") + " - " +getIntent().getStringExtra("objektName");
                    String body = getApplicationContext().getString(R.string.body_buchen);
                    String token ="ex9Ytii5KbI:APA91bHaWZy27CTBYd9jbFQWICXMMXetcsF1oKsI8f8_vs8J5ktIzTqM80IEzsEmOZCiDK3w3RkoXu3f8N1rlziSYZEL0O-ddxMaKWPBrCeQStcv9ZiqEHttMuILenCBPv94mLgDuVac";

                    //Notification Methode1
                     NotificationsHelper.displayNotification(getApplicationContext(),title,body,token);
                    // Methode 2: new SendNotificationActivity().sendNotification(title,body,token);
                    //Method 3
                   // new NotificationsService();


                }else{
                    Toast.makeText(getApplicationContext(),"you must first connect",Toast.LENGTH_LONG).show();
                }



            }
        });
        // ######################################### Ende Speichern ##################################################################################

        btnAbbrechen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(),HomeActivity.class);
                startActivity(intent);

            }
        });

    }
    private boolean save_bestellung(User user, final String userId_firebase, final String id_objekt_bestellt){
        listeUser = new ArrayList<>();

        //recuperation des info du user
        mDatabaseRef_user = FirebaseDatabase.getInstance().getReference("user");
        mDatabaseRef_user.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                //okay
                for(DataSnapshot postSnapshot : dataSnapshot.getChildren()){
                    user_recupere_firebase = postSnapshot.getValue(User.class);
                    if(userId_firebase.equals(postSnapshot.getKey())){

                        //id jeder Objekt
                        User user_recupere_firebase_ = postSnapshot.getValue(User.class);
                        listeUser.add(user_recupere_firebase_);
                        //#################################################################################################
                        if( listeUser.size()>0){
                            String resultat="null";

                            User u=listeUser.get(0);
                            User neu_user_to_save =new User();

                            neu_user_to_save.setId(userId_firebase);
                            neu_user_to_save.setUserId_firebase(userId_firebase);
                            neu_user_to_save.setName(u.getName());
                            neu_user_to_save.setEmail(u.getEmail());
                            neu_user_to_save.setAdresse(u.getAdresse());
                            neu_user_to_save.setObjekt(u.getObjekt());
                            neu_user_to_save.setProfil(u.getProfil());
                            neu_user_to_save.setToken_user(u.getToken_user());
                            neu_user_to_save.setBestellung(u.getBestellung());
                            if(u.getBestellung().equals("null")){

                                String bestellung_user = "[{\"id_objekt_bestellt\":\""+id_objekt_bestellt+"\"}]";

                                neu_user_to_save.setBestellung(bestellung_user);

                                DatabaseReference dR = FirebaseDatabase.getInstance().getReference("user").child(userId_firebase);
                                dR.setValue(neu_user_to_save);
                                resultat="new_command";

                            }else{
                                try {
                                    JSONArray liste_bestellungen= new JSONArray(u.getBestellung());
                                    int index=0;
                                    String resultat_alle_and_new_bestellung="[";


                                    while(index<liste_bestellungen.length()){



                                        JSONObject bestellung=  liste_bestellungen.getJSONObject(index);


                                        if (bestellung.getString("id_objekt_bestellt").equals(id_objekt_bestellt)){

                                            resultat="exist";
                                            Toast.makeText(getApplicationContext(), " You already ordered it.",Toast.LENGTH_LONG).show();
                                        }else{

                                            resultat_alle_and_new_bestellung=resultat_alle_and_new_bestellung+"{\"id_objekt_bestellt\":\""+bestellung.getString("id_objekt_bestellt")+"\"},";
                                        }



                                        if(index==liste_bestellungen.length()-1 && resultat.equals("null")){

                                            resultat_alle_and_new_bestellung=resultat_alle_and_new_bestellung+"{\"id_objekt_bestellt\":\""+id_objekt_bestellt+"\"}]";

                                            neu_user_to_save.setBestellung(resultat_alle_and_new_bestellung);

                                            DatabaseReference dR = FirebaseDatabase.getInstance().getReference("user").child(userId_firebase);
                                           dR.setValue(neu_user_to_save);
                                            resultat="new_command";

                                        }
                                        index++;


                                    }



                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }
                            if(resultat.equals("new_command")){
                                Intent intent = new Intent(ObjektDetailActivity.this,HomePageUser.class);

                                startActivity(intent);

                            }else{

                            }
                            listeUser.remove(u);

                        }
                        //######################################################################################################################

                    }

                }

                // Wenn objekts erstellen, dann create adapter



            }



            @Override
            public void onCancelled(DatabaseError databaseError) {
                //error fixed when eg Internet or Permission

            }
        });



        return true;
    }
    private void pictureZoom(String objektName, String objektImage){
        Intent intent = new Intent(ObjektDetailActivity.this,PictureZoom.class);
        intent.putExtra("objektName", objektName);
        intent.putExtra("objektImage",objektImage);
        startActivity(intent);

    }

    private void makePhoneCall() {
        //String number= "017677420641";
        if(phone.trim().length()>0){

            if(ContextCompat.checkSelfPermission(ObjektDetailActivity.this,
                    Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED){
                ActivityCompat.requestPermissions(ObjektDetailActivity.this,
                        new String[]{Manifest.permission.CALL_PHONE},REQUEST_CALL);
            } else{
                String dial = "tel:" + phone;
                startActivity(new Intent(Intent.ACTION_CALL,Uri.parse(dial)));
            }
        }else {
            Toast.makeText(this,"Nummer ist nicht vergeben",Toast.LENGTH_SHORT).show();
//todo wenn nummber vergeben ist
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_CALL){
            if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                makePhoneCall(); //call againt
            } else{
                Toast.makeText(this,"Permission DENIED, FundObjekt TEAM",Toast.LENGTH_SHORT).show();
            }
        }
    }

    //convertor Date
    public static String getDateErstellen(Date date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "EEE, d MMM yyyy HH:mm", Locale.getDefault());
        return dateFormat.format(date);
    }

}

