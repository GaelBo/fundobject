package com.example.lawe.fundobject.create_object;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ProgressBar;
import android.widget.SearchView;
import android.widget.TextView;

import com.example.lawe.fundobject.adapters_helper.ImageAdapter;
import com.example.lawe.fundobject.home_user.HomePageUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;


import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;

import java.text.SimpleDateFormat;
import java.util.Date;

import java.util.List;

import com.example.lawe.fundobject.R;
import com.example.lawe.fundobject.konfiguration.Einstellung;
import com.example.lawe.fundobject.konfiguration.Notification;
import com.example.lawe.fundobject.konfiguration.Rechtline;
import com.example.lawe.fundobject.registrierung_anmeldung.SessionUser;

import com.example.lawe.fundobject.models.Objekt;
import com.example.lawe.fundobject.registrierung_anmeldung.*;

import java.util.ArrayList;
import java.util.Locale;

import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import static com.facebook.FacebookSdk.getApplicationContext;

public class HomeActivity extends AppCompatActivity implements ImageAdapter.OnItemClickListener, Filterable {

    private RecyclerView mRecyclerView;
    private ImageAdapter mAdapter;

    private ProgressBar mProgressCircle;
    private  TextView textView_name_user_home;
    private DatabaseReference mDatabaseRef;
    private List<Objekt>mObjekts;
    private List<Objekt>mObjektsFull;
    private List<String>mIdObjekts;
    String idObjekt;
    private   String name,email_user;
    SessionUser sessionUser;
    SearchView seach_objekt_home;
    //Vars session
    String emailCurrent;
    int id_user;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        mRecyclerView =(RecyclerView) findViewById(R.id.recycler_view);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mProgressCircle = findViewById(R.id.progress_circle);
        seach_objekt_home = (SearchView) findViewById(R.id.seach_objekt_home);
        textView_name_user_home= findViewById(R.id.textView_name_user_home);

        sessionUser = new SessionUser(getApplicationContext());
        if(sessionUser.isLoggedIn()) {
              sessionUser.checkLogin();
            // get user data from session
            HashMap<String, String> user = sessionUser.getUserDetails();

            // name
              name = user.get(SessionUser.KEY_NAME);

            // email
              email_user = user.get(SessionUser.KEY_EMAIL);

            // displaying user data
            try {
                JSONObject info_user=new JSONObject(name);
              String  userId_firebase=info_user.getString("userId_firebase");
              name=info_user.getString("name");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            textView_name_user_home.setText(Html.fromHtml("<p> <b>" + name + "</b><p>"));
            //btnFacebook.setText(Html.fromHtml("Email: <b>" + email + "</b>"));

        }else{
            textView_name_user_home.setText(Html.fromHtml("."));
        }



        mObjekts = new ArrayList<>(); //weil mit Array ist mor flexibel stadt List<>
        mIdObjekts = new ArrayList<>(); //um Id jede Objekt zu speeichern
        mObjektsFull =new ArrayList<>(); //to search

        mDatabaseRef = FirebaseDatabase.getInstance().getReference("Objekt");



        mDatabaseRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                //okay
                for(DataSnapshot postSnapshot : dataSnapshot.getChildren()){
                    Objekt objekt = postSnapshot.getValue(Objekt.class);
                     idObjekt = postSnapshot.getKey(); //id jeder Objekt

                    mIdObjekts.add(idObjekt); //ID firebase store use
                    mObjekts.add(objekt);
                    mObjektsFull.add(objekt); // to search

                }
                // Wenn objekts erstellen, dann create adapter
                // Sortierung
                Collections.reverse(mIdObjekts);
                Collections.reverse(mObjektsFull);
                Collections.reverse(mIdObjekts);
                // und zeigen
              displayObjekt(mObjekts);


            }



            @Override
            public void onCancelled(DatabaseError databaseError) {
                //error fixed when eg Internet or Permission
                Toast.makeText(HomeActivity.this, databaseError.getMessage(),Toast.LENGTH_SHORT).show();
                mProgressCircle.setVisibility(View.INVISIBLE);
            }
        });

    }

    public void displayObjekt( List<Objekt>myObj) {
        // Wenn objekts erstellen, dann create adapter
        mAdapter = new ImageAdapter(HomeActivity.this,myObj);

        mRecyclerView.setAdapter(mAdapter);
        mAdapter.setOnItemClickListener(HomeActivity.this);
        mProgressCircle.setVisibility(View.INVISIBLE);

    }




    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:

                    return  true;
                case R.id.navigation_erstellen:

                      sessionUser = new SessionUser(getApplicationContext());
                    if(sessionUser.isLoggedIn()) {

                        Intent intent2 = new Intent(HomeActivity.this, ObjektErstellenActivity.class);
                        startActivity(intent2);
                    }else{

                        Intent intent_kont = new Intent(HomeActivity.this, Registrationlogin.class);
                        startActivity(intent_kont);

                    }

                    return  true;
                case R.id.navigation_konto:

                    sessionUser = new SessionUser(getApplicationContext());
                    if(sessionUser.isLoggedIn()) {

                        Intent intent_kont = new Intent(HomeActivity.this, HomePageUser.class);
                        startActivity(intent_kont);

                    }else{
                        Intent intent2_ = new Intent(HomeActivity.this, Registrationlogin.class);
                        startActivity(intent2_);

                    }
                    return true;

            }
            return false;
        }
    };



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.search_menu, menu);

        // to search implementieren
        MenuItem searchObjekt = menu.findItem(R.id.seach_objekt_home);
        SearchView searchView = (SearchView) searchObjekt.getActionView();
        searchView.setImeOptions(EditorInfo.IME_ACTION_DONE); //search another

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
               // mAdapter.getFilter().filter(newText);
                getFilter().filter(newText);
                return false;
            }
        });
        return true;
    }


    public boolean onCreateOptionsMenu2(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.search_menu, menu);
        return true;
    }



    public boolean onOptionsItemSelected(MenuItem item) {
        int id= item.getItemId();
        if (id==R.id.action_notifications){
            Intent intent= new Intent(HomeActivity.this,Notification.class);
            startActivity(intent);
        }
        else
        if (id==R.id.action_rechtlicheHinweise){
            Intent intent= new Intent(HomeActivity.this,Rechtline.class);
            //Intent intent= new Intent(MainActivity.this,ObjektErstellenActivity.class);
            startActivity(intent);
        }
        else
        if (id==R.id.action_settings){
            Intent intent= new Intent(HomeActivity.this,Einstellung.class);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onItemClick(int position) {
        Intent intent;
        String phone = null;
        String emailTmp = mObjekts.get(position).getId_ersteller();
       // String key = mDatabaseRef.child("Objekt").push().getKey();
       final String objetktID = mIdObjekts.get(position);

        try {
            JSONObject tmp = new JSONObject(emailTmp);
            emailTmp = new String(tmp.getString("email"));
            phone = new String(tmp.getString("phone"));
        }catch (JSONException j){}
       // Toast.makeText(HomeActivity.this,emailTmp, Toast.LENGTH_LONG).show();
       // Toast.makeText(HomeActivity.this,objetktID, Toast.LENGTH_LONG).show();

        if (emailTmp.equals(email_user)) {
             intent = new Intent(HomeActivity.this,DetailMyObjektActivity.class);
        } else {
           // intent = new Intent(HomeActivity.this,DetailMyObjektActivity.class);
            intent = new Intent(HomeActivity.this,ObjektDetailActivity.class);
        }
        intent.putExtra("objektID",objetktID);
        intent.putExtra("objektImage",  mObjekts.get(position).getImage());
       // intent.putExtra("objektType",  mObjekts.get(position).getType); //  fixme  false or true
        intent.putExtra("objektName",  mObjekts.get(position).getName());
        intent.putExtra("objektKategorie",  mObjekts.get(position).getKategorie()).toString();
        //Convertion to String with String.valueOf()
        String dateString = getDateErstellen(mObjekts.get(position).getDate_objekt_erstellen());
        intent.putExtra("objektPrice", String.valueOf( mObjekts.get(position).getPrice()));
        intent.putExtra("objektDate", dateString); // todo
        //Toast.makeText(getApplicationContext(),dateString,Toast.LENGTH_LONG).show();
        intent.putExtra("objektBeschreibung",  mObjekts.get(position).getBeschreibung());
        intent.putExtra("idErsteller",  mObjekts.get(position).getId_ersteller());
        intent.putExtra("Phone", phone  );
        intent.putExtra("objektAdresse",  mObjekts.get(position).getPlz().concat(mObjekts.get(position).getStrasse()));
        intent.putExtra("objektPlz",  mObjekts.get(position).getPlz());
        intent.putExtra("objektStrasse",  mObjekts.get(position).getStrasse());
        intent.putExtra("objektStadt",  mObjekts.get(position).getStadt());
        intent.putExtra("objektDate",  mObjekts.get(position).getDate_objekt_erstellen());

        startActivity(intent);
    }

    /*
    **to search implementiren wir Filterable interface
     */
    @Override
    public Filter getFilter() {
        return objektFilter;
    }
    private Filter objektFilter = new Filter() {
        /*
        @constraint ist der ParamCurrent für Sortierung
         */
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<Objekt> filteredList = new ArrayList<>();
            if (constraint == null || constraint.length() == 0) {
                filteredList.addAll(mObjektsFull);
            } else {
              String filterPattern = constraint.toString().toLowerCase().trim();
                // Sortierung nach Name, Kategorie, Plz, Stadt, Strasse
              for (Objekt objekt: mObjektsFull) {
                  if (objekt.getName().toLowerCase().contains(filterPattern) ||
                          objekt.getKategorie().toLowerCase().contains(filterPattern) ||
                          objekt.getPlz().toLowerCase().contains(filterPattern) ||
                          objekt.getStadt().toLowerCase().contains(filterPattern) ||
                          objekt.getStrasse().toLowerCase().contains(filterPattern)) {
                      filteredList.add(objekt);
                  }
              }
            }
            //nach Sortirung return wirs filtererdList durch results variable
            FilterResults results = new FilterResults();
            results.values = filteredList;

            return  results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            //wir muessen jetzt alte LIST löschen,  neu LISTObjekts add
            mObjekts.clear();
            mObjekts.addAll((List) results.values);
            // Toast.makeText(HomeActivity.this, "Sortierung functioniert gut aber Adapter muss refrescht werden ", Toast.LENGTH_LONG).show();
          // mAdapter.notifyDataSetChanged(); //refrech der Adapter // fixme
            displayObjekt(mObjekts);

        }
    };
    //convertor Date
    public static String getDateErstellen(Date date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "EEE, d MMM yyyy HH:mm", Locale.getDefault());
        return dateFormat.format(date);
    }
}
