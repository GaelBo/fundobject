package com.example.lawe.fundobject.models;


public class User {
    String id;
    public String name;
    public String email;
    public String password;
    public String adresse;
    public   String objekt = "null";
    public   String bestellung = "null";
    public   String profil = "null";
    public  String userId_firebase="null";
    public  String token_user="null";

    public User() {
    }
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public String getUserId_firebase() {
        return userId_firebase;
    }
    public void setUserId_firebase(String userId_firebase) { this.userId_firebase = userId_firebase; }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getToken_user() {
        return token_user;
    }
    public void setToken_user(String token_user) {
        this.token_user = token_user;
    }

    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    public String getBestellung() { return bestellung; }
    public void setBestellung(String bestellung) {
        this.bestellung = bestellung;
    }

    public String getProfil() { return profil; }
    public void setProfil(String profil) {
        this.profil = profil;
    }


    public String getAdresse() {
        return adresse;
    }
    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getObjekt() { return objekt; }
    public void setObjekt(String objekt) {
        this.objekt = objekt;
    }


}