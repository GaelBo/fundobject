package com.example.lawe.fundobject.registrierung_anmeldung;
import com.example.lawe.fundobject.models.User;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.DOMStringList;

import java.util.ArrayList;
import java.util.Arrays;
import java.lang.Object;





public class DBManager extends SQLiteOpenHelper {


    private static final String TAG = "DatabaseHelper";

    private static final String DATABASE = "fundobjekt";
    private static final String TABLE_NAME = "users";
    private static final String COL1 = "INFOUSER";
    private static final String COL2 = "BILDER";


    public DBManager(Context context) {

        super(context, TABLE_NAME, null, 1);

    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String createTable = "";
        db.execSQL("create table  "+TABLE_NAME+"(ID INTEGER PRIMARY KEY AUTOINCREMENT,INFOUSER TEXT,BILDER TEXT)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF  EXISTS " + TABLE_NAME);
        onCreate(db);
    }

    public boolean addData(User u){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        String name="";
       // String info_user=u.name+","+u.email+","+u.password;
        String info_user = "[{\"name\":\""+u.name+"\", " +
                             "\"email\":\""+u.email+"\"," +
                             "\"password\":\""+u.password+"\"," +
                             "\"adresse\":\""+u.adresse+"\"," +
                             "\"objekt\":\""+u.objekt+"\"," +
                             "\"userId_firebase\":\""+u.userId_firebase+"\"," +
                             "\"bestellung\":\""+u.bestellung+"\"," +
                             "\"profil\":\""+u.profil+"\"}]";

        contentValues.put(COL1, info_user);
        contentValues.put(COL2,"[{name:cesaire}]");

        Log.d(TAG, "addData: Adding " +u+ " to userfundobjet");

        long result = db.insert(TABLE_NAME, null, contentValues);

        //if date as inserted incorrectly it will return -1
        if (result == -1) {
            return false;
        } else {
            return true;
        }

    }

    /**
     * Returns all the data from database
     * @return
     */
    public Cursor getData(){
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT * FROM "+TABLE_NAME;
        //Cursor data = db.rawQuery(query, null);
      Cursor  cursor = db.rawQuery("SELECT * FROM "+this.TABLE_NAME+"", null);

        return cursor;
    }


    public  JSONObject verifiertExisteUser(String email, String password){


        ArrayList<String> listData = new ArrayList<>();
        ArrayList<String> liste_tmp = new ArrayList<>();
        boolean  find_user=false;
        int id_user=0;
        String email_user;
        String name="";
        String password_user;

        JSONObject resultat=null;
         Cursor data = getData();
        while(data.moveToNext()){

            String tmp=data.getString(1); // dieses Colorn enthält  alle Infomation des User als Liste
            int id=data.getInt(0);
             try {
                JSONArray js= new JSONArray(tmp);
                 int index=0;
                 while(index<js.length()){
                     JSONObject object=  js.getJSONObject(index);
                     email_user=new String(object.getString("email"));
                     password_user=new String(object.getString("password"));

                     if(email_user.equals(email)){

                         if(password_user.equals(password)){
                             name=new String(object.getString("name"));
                             String mess = "{\"id\":\""+id+"\", " +
                                     "\"id_test\":\"ok\"," +
                                     "\"name\":\""+name+"\"," +
                                     "\"userId_firebase\":\""+object.getString("userId_firebase")+"\"," +
                                     "\"message\":\"connection successful\"}";
                               resultat= new JSONObject(mess);
                               break;
                         }else{
                             int nulle=0;
                             String mess = "{\"id\":\""+nulle+"\", " +
                                     "\"id_test\":\"null\"," +
                                     "\"name\":\"null\"," +
                                     "\"message\":\"Error  password ist not correct ! try again\"}";
                             resultat= new JSONObject(mess);
                             break;
                         }

                     }else{
                            int nulle=0;
                            String mess = "{\"id\":\""+nulle+"\", " +
                                    "\"id_test\":\"null\"," +
                                    "\"name\":\"null\"," +
                                    "\"message\":\"Error  user not exist ! try again\"}";
                              resultat= new JSONObject(mess);
                        }


                     index++;

                 }
            } catch (JSONException e) {
                e.printStackTrace();

            }




        }
        return resultat;

    }

    /**
     * Returns only the ID that matches the name passed in
     * @param name
     * @return
     */
    public Cursor getItemID(String name){
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT " + COL1 + " FROM " + TABLE_NAME +
                " WHERE " + COL2 + " = '" + name + "'";
        Cursor data = db.rawQuery(query, null);
        return data;
    }

    public  int search_id_user(String email,String password){

        int id_user=0;
        String email_user, password_user;
        Cursor data = getData();
        while(data.moveToNext()){
            String tmp=data.getString(1);
            try {
                JSONArray js= new JSONArray(tmp);
                int index=0;
                while(index<js.length()){
                    JSONObject object=  js.getJSONObject(index);
                    email_user=new String(object.getString("email"));
                    password_user=new String(object.getString("password"));
                    if(email_user.equals(email)){
                        if(password_user.equals(password)){
                            id_user=data.getInt(0);
                            break;
                        }
                    }
                    index++;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return id_user;
    }
    /**
     * Updates the name field
     * @param newName
     * @param id
     * @param oldName
     */
    public void updateName(String newName, int id, String oldName){
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "UPDATE " + TABLE_NAME + " SET " + COL2 +
                " = '" + newName + "' WHERE " + COL1 + " = '" + id + "'" +
                " AND " + COL2 + " = '" + oldName + "'";
        Log.d(TAG, "updateName: query: " + query);
        Log.d(TAG, "updateName: Setting name to " + newName);
        db.execSQL(query);
    }


    public void deleteAll( ){
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "DELETE FROM "+TABLE_NAME;
        Log.d(TAG, "deleteName: query: " + query);
        Log.d(TAG, "deleteName: Deleting all from database.");
        db.execSQL(query);
    }
}
