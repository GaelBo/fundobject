package com.example.lawe.fundobject.registrierung_anmeldung;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBsqlite_user extends SQLiteOpenHelper {


    public static String DATABASE_NAME = "FundObjekt";
    public static final String TABLE_NAME1 = "ObjektTable";
    public static final String TABLE_NAME2 = "ObjektTable_Image";
    public static final String TABLE_NAME = "userTableV0";

    public static final String Table_Column_ID = "id";
    public static final String Table_Column_1_Name = "name";
    public static final String Table_Column_2_Kategorie = "kategorie";
    public static final String Table_Column_3_Price = "price";
    public static final String Table_Column_4_Beschreibung = "beschreibung";
    public static final String Table_Column_5_Bild = "bild";


    private String CREATE_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + " (" + Table_Column_ID + " INTEGER PRIMARY KEY, " + Table_Column_1_Name + " VARCHAR, " + Table_Column_2_Kategorie + " VARCHAR, " + Table_Column_3_Price + " VARCHAR," + Table_Column_4_Beschreibung + " VARCHAR)";

    //***************************************************************************************************

    public DBsqlite_user(Context context) {

        super(context, DATABASE_NAME, null, 1);

    }

    @Override
    public void onCreate(SQLiteDatabase database) {


        database.execSQL(CREATE_TABLE);


    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);

    }

}