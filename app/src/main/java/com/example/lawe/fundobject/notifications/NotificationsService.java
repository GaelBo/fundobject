package com.example.lawe.fundobject.notifications;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.example.lawe.fundobject.MainActivity;
import com.example.lawe.fundobject.R;
import com.example.lawe.fundobject.create_object.HomeActivity;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

public class NotificationsService extends FirebaseMessagingService {

//    private final int NOTIFICATION_ID = 007;
//    private final String NOTIFICATION_TAG = "FIREBASEOC";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        if (remoteMessage.getNotification() != null) {
            String title = remoteMessage.getNotification().getTitle();
            String body = remoteMessage.getNotification().getBody();
            String token ="ex9Ytii5KbI:APA91bHaWZy27CTBYd9jbFQWICXMMXetcsF1oKsI8f8_vs8J5ktIzTqM80IEzsEmOZCiDK3w3RkoXu3f8N1rlziSYZEL0O-ddxMaKWPBrCeQStcv9ZiqEHttMuILenCBPv94mLgDuVac";


            NotificationsHelper.displayNotification(getApplicationContext(),title,body, token);
        }
    }

//    // ---
//    private void sendVisualNotification(String messageBody) {
//
//        // 1 - Create an Intent that will be shown when user will click on the Notification
//        Intent intent = new Intent(this, HomeActivity.class);
//        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);
//
//        // 2 - Create a Style for the Notification
//        NotificationCompat.InboxStyle inboxStyle = new NotificationCompat.InboxStyle();
//        inboxStyle.setBigContentTitle(getString(R.string.notification_title));
//        inboxStyle.addLine(messageBody);
//
//        // 3 - Create a Channel (Android 8) //Kanal
//        String channelId = getString(R.string.default_notification_channel_id);
//
//        // 4 - Build a Notification object
//        NotificationCompat.Builder notificationBuilder =
//                new NotificationCompat.Builder(this, channelId)
//                        .setSmallIcon(R.mipmap.ic_launcher)
//                        .setContentTitle(getString(R.string.app_name))
//                        .setContentText(getString(R.string.notification_title))
//                        .setAutoCancel(true)
//                        .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
//                        .setContentIntent(pendingIntent)
//                        .setStyle(inboxStyle);
//
//        // 5 - Add the Notification to the Notification Manager and show it to m
//        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//
//        // 6 - Support Version >= Android 8
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//            CharSequence channelName = "Message provenant de Firebase - FundObjectTEAM";
//            int importance = NotificationManager.IMPORTANCE_HIGH;
//            NotificationChannel mChannel = new NotificationChannel(channelId, channelName, importance);
//            notificationManager.createNotificationChannel(mChannel);
//        }
//
//        // 7 - Show notification
//        notificationManager.notify(NOTIFICATION_TAG, NOTIFICATION_ID, notificationBuilder.build());
//    }
}