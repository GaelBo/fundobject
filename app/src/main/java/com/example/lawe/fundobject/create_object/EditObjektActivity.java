package com.example.lawe.fundobject.create_object;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.example.lawe.fundobject.MainActivity;
import com.example.lawe.fundobject.R;
import com.example.lawe.fundobject.adapters_helper.SQLiteHelper;
import com.example.lawe.fundobject.models.Objekt;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.StorageTask;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Calendar;
import java.util.Date;
import java.util.StringTokenizer;

import static android.app.Activity.RESULT_OK;
import static android.provider.Telephony.Mms.Part.TEXT;

public class EditObjektActivity extends AppCompatActivity{


    // Write a message to the database realtime
    //FirebaseDatabase database = FirebaseDatabase.getInstance();
    private DatabaseReference mDatabaseRef;
    private StorageReference mStorageRef;
    private StorageTask mObjektTask; //und nur ein Objekt zu speichern beim mehr clicks add

    // Access a Cloud Firestore instance from your Activity6
    FirebaseFirestore db_firestore = FirebaseFirestore.getInstance();

    SQLiteDatabase sqLiteDatabaseObj;
    SQLiteHelper dbHelper; //vllt zu löschen
    EditText editTextName,editTextPrice,editTextBeschreibung, editTextPlz,editTextStasse,
            editTextStadt, editTextIdErsteller,editTextListBesteller, editTextPhone;
    Date dateErstellen;
    EditText editTextImageAdresse; //chemin uri de  l image //fixme todo
    RadioButton radioButtonType;
    Spinner spinnerKategorie;
    Boolean EditTextEmptyHold, booleanEtat;
    //////////////////////////////////////////////////////////
    String NameHolder;
    String KategorieHolder;
    String PriceHolder;
    String BeschreibungHolder;
    String SQLiteDataBaseQueryHolder;
    boolean TypeHolder;
    String EtatHolder;
    String PlzHolder;
    String StrasseHolder;
    String StadtHolder;
    String IdErstellerHolder; // Email/Phone
    String ListIdBestellerHolder; //EmailBestellers
    String PhoneHolder;
    String ImageAdresseHolder;
    Button EnterData, ButtonAbrechen;
    RadioGroup radioGroup;
    ProgressBar mProgressBar;
    int selectedId; //für radiogroud select elt
    //image tuto: saving Retrieving images from SQLite Database in Android Part 1
    //https://www.youtube.com/watch?v=X7T0g5kBYJk
    private ImageView mImageView;
    private static final int SELECT_PHOTO = 1;
    private static final int CAPTURE_PHOTO = 2;
    private Uri mImageUri;

    private ProgressDialog progressBar;
    private int progressBarStatus=0;
    private Handler progressBarHandler=new Handler();
    private boolean hasImageChanged=false;
    Bitmap thumbnail;

//    //Var fuer Session User und Share Preference
//    SharedPreferences sharedPreferences;
//    String name_user;
//    String email_user;
//    String psswrd;
//    private Switch switch1;
//    public static final String SHARED_PREFS = "sharedPrefs";
//    public static final String PHONE = "phone";
//    public static final String SWITCH1 = "switch1";
//    private boolean switchOnOff;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_objekt_erstellen);
//        name_user= getIntent().getStringExtra("name");
//        email_user= getIntent().getStringExtra("email");
//        int id_user=getIntent().getIntExtra("id_user",0);

        //Toast.makeText(EditObjektActivity.this,id_user+"-"+email_user+"-"+name_user, Toast.LENGTH_SHORT).show();

        EnterData = (Button)findViewById(R.id.edit_btn_add_objekt);
        ButtonAbrechen = (Button)findViewById(R.id.edit_btn_abbrechen);
        mProgressBar = (ProgressBar)findViewById(R.id.edit_input_progress_bar);
        editTextName = (EditText)findViewById(R.id.edit_input_name);
        spinnerKategorie=(Spinner)findViewById(R.id.edit_input_spinner_kategorie);
        radioGroup=(RadioGroup)findViewById(R.id.edit_group);
//        selectedId = radioGroup.getCheckedRadioButtonId();
//        radioButtonType = (RadioButton) findViewById(selectedId);
        booleanEtat=true; //Objekt ist beim Erstellen direkt verfügbar
        editTextPrice = (EditText)findViewById(R.id.edit_input_Price);
        editTextBeschreibung = (EditText)findViewById(R.id.edit_input_beschreibung);
        editTextPlz=(EditText)findViewById(R.id.edit_input_plz);
        editTextStasse=(EditText)findViewById(R.id.edit_input_strasse);
        editTextStadt=(EditText)findViewById(R.id.edit_input_stadt);
        editTextPhone = (EditText) findViewById(R.id.edit_input_phone);
        editTextIdErsteller=null; //fixme me
        editTextListBesteller=null;
        dateErstellen=Calendar.getInstance().getTime();//new Date();//aktuelle Datum
        mImageView = (ImageView)findViewById(R.id.edit_input_image);

        // fuer Share Preference
      //  switch1 = (Switch) findViewById(R.id.edit_switch1);

        mStorageRef = FirebaseStorage.getInstance().getReference("Objekt");
        mDatabaseRef = FirebaseDatabase.getInstance().getReference("Objekt");

        mImageView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                if (ContextCompat.checkSelfPermission(EditObjektActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    mImageView.setEnabled(false);
                    ActivityCompat.requestPermissions(EditObjektActivity.this, new String[] { Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE }, 0);
                } else {
                    mImageView.setEnabled(true);
                }
                taskImage(view); // fixme when crash
            }
        });

        EnterData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Firestore code
                //avant de add , il faut checken
                if(mObjektTask != null && mObjektTask.isInProgress()){
                    Toast.makeText(EditObjektActivity.this,"Image in progress", Toast.LENGTH_SHORT).show();
                } else {
                    addObjekt();
                    EnterData.setEnabled(false);
                    Intent i = new Intent(EditObjektActivity.this, HomeActivity.class);
                    startActivity(i);
                }

            }
        });

        ButtonAbrechen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(EditObjektActivity.this, HomeActivity.class);
                startActivity(intent);
            }
        });

//        // fuer SharedPreference
//        loadData();
//        updateViews();

    } //en onCreate

    //für image convertion
    private String getFileExtension(Uri uri){
        ContentResolver cR = getContentResolver();
        MimeTypeMap mime = MimeTypeMap.getSingleton();
        return mime.getExtensionFromMimeType(cR.getType(uri));
    }

    public void addObjekt() {
        if (mImageUri != null) {
            final StorageReference fileReference = mStorageRef.child(System.currentTimeMillis() + "." + getFileExtension(mImageUri));

            mObjektTask = fileReference.putFile(mImageUri)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    mProgressBar.setProgress(0);
                                }
                            }, 500);
                            fileReference.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                @Override
                                public void onSuccess(Uri uri) {
                                    NameHolder = editTextName.getText().toString().trim() ;
                                    KategorieHolder=spinnerKategorie.getSelectedItem().toString().trim();
                                    PriceHolder = editTextPrice.getText().toString().trim() ;
                                    float price= Float.parseFloat(PriceHolder);
                                    BeschreibungHolder = editTextBeschreibung.getText().toString().trim() ;
                                    selectedId = radioGroup.getCheckedRadioButtonId();
                                    TypeHolder=true;
                                    int selectedId = radioGroup.getCheckedRadioButtonId();
                                    radioButtonType = (RadioButton) findViewById(selectedId);
                                    EtatHolder= String.valueOf(radioButtonType); // verloren, verleihen oder tauschen
                                    PlzHolder=editTextPlz.getText().toString().trim();
                                    StrasseHolder=editTextStasse.getText().toString().trim();
                                    StadtHolder=editTextStadt.getText().toString().trim();
                                    PhoneHolder = editTextPhone.getText().toString().trim() ;
                                    //String PhoneNr= Long.getLong(PhoneHolder); //fixme or todo wenn crash
//                                    IdErstellerHolder="{\"email\":\""+email_user+"\"," +
//                                            "           \"phone\":\""+PhoneHolder+"\"}"; //email_user
                                    IdErstellerHolder = null ;
                                    ListIdBestellerHolder=null;
                                    //long[]list=null;
                                    dateErstellen=Calendar.getInstance().getTime();

                                    ImageAdresseHolder = uri.toString(); //Fixme new Technick

                                    Objekt objekt=new Objekt(
                                            NameHolder,
                                            KategorieHolder,
                                            EtatHolder,
                                            true,
                                            price,
                                            BeschreibungHolder,
                                            PlzHolder,
                                            StrasseHolder,
                                            StadtHolder,
                                            IdErstellerHolder,
                                            ListIdBestellerHolder,
                                            ImageAdresseHolder,
                                            dateErstellen);
                                    // todo try connection internet
                                    String objektId = mDatabaseRef.push().getKey();
                                    mDatabaseRef.child(objektId).setValue(objekt);
//                                    /*
//                                     **@Param Phone local SharedPreferences
//                                     */
//                                    sharedPreferences = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
//                                    SharedPreferences.Editor editor = sharedPreferences.edit();
//                                    editor.putString(TEXT, NameHolder);
//                                    editor.putBoolean(SWITCH1, switch1.isChecked());
//                                    editor.apply();
//                                    // End SharedPrefrence

                                    Toast.makeText(EditObjektActivity.this, "Object  successfully", Toast.LENGTH_LONG).show();
                                    // imgPreview.setImageResource(R.drawable.imagepreview);
                                    // imgDescription.setText("");
                                }
                            });


                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Toast.makeText(EditObjektActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            double progress = (100.0 * taskSnapshot.getBytesTransferred() / taskSnapshot.getTotalByteCount());
                            mProgressBar.setProgress((int) progress);
                        }
                    });
        } else {
            Toast.makeText(EditObjektActivity.this, "No file selected", Toast.LENGTH_SHORT).show();
        }
    } //end new code


    //Test UNIText todo
    private boolean validateInputsObjekt(Objekt objekt) {
        {
            if (objekt.getName()!=null) return true;
            else return false;
        }
    }

    //***********fonction pour SQLite*************
    public void SQLiteDataBaseBuild(){

        sqLiteDatabaseObj = openOrCreateDatabase(SQLiteHelper.DATABASE_NAME, Context.MODE_PRIVATE, null);

    }

    public void CheckEditTextStatus(){

        NameHolder = editTextName.getText().toString() ;
        // KategorieHolder = editTextKategorie.getText().toString() ;
        KategorieHolder=spinnerKategorie.getSelectedItem().toString();
        PriceHolder = editTextPrice.getText().toString() ;
        BeschreibungHolder = editTextBeschreibung.getText().toString() ;

        //HIER test TODO
        if(TextUtils.isEmpty(NameHolder) || TextUtils.isEmpty(PriceHolder)){

            EditTextEmptyHold = false ;

        }
        else {

            EditTextEmptyHold = true ;
        }
    }

    public void InsertDataIntoSQLiteDatabase(){

        if(EditTextEmptyHold == true)
        {

            SQLiteDataBaseQueryHolder = "INSERT INTO "+SQLiteHelper.TABLE_NAME+" (name,kategorie,price,beschreibung) VALUES('"+NameHolder+"', '"+KategorieHolder+"', '"+PriceHolder+"', '"+BeschreibungHolder+"');";

            sqLiteDatabaseObj.execSQL(SQLiteDataBaseQueryHolder);

            sqLiteDatabaseObj.close();

            Toast.makeText(this,"Data Inserted Successfully", Toast.LENGTH_LONG).show();

        }
        else {

            Toast.makeText(this,"Please Fill All The Required Fields.", Toast.LENGTH_LONG).show();

        }

    }

    public void EmptyEditTextAfterDataInsert(){

        editTextName.getText().clear();
        // editTextKategorie.getText().clear();
        editTextPrice.getText().clear();
        editTextBeschreibung.getText().clear();

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(requestCode == 0) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED
                    && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                mImageView.setEnabled(true);

            }
        }
    }

    public void setProgressBar(){
        progressBar=new ProgressDialog(this);
        progressBar.setCancelable(true);
        progressBar.setMessage("Please wait ... ");
        progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressBar.setProgress(0);
        progressBar.setMax(100);
        progressBar.show();
        progressBarStatus=0;
        new Thread(new Runnable() {
            @Override
            public void run() {
                while(progressBarStatus < 100){
                    progressBarStatus += 50;
                    try{
                        Thread.sleep(300);
                    }catch (InterruptedException e){
                        e.printStackTrace();
                    }
                    progressBarHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            progressBar.setProgress(progressBarStatus);
                        }
                    });
                }
                if(progressBarStatus>=100){
                    try {
                        Thread.sleep(2000);
                    }catch (InterruptedException e){
                        e.printStackTrace();
                    }
                    progressBar.dismiss();
                }
            }
        }).start();

    } //end setProgressBar()


    private void taskImage(View view1){

        switch (view1.getId()) {

            case R.id.input_image:
                new MaterialDialog.Builder(this)
                        .title(R.string.uploadImages)
                        .items(R.array.uploadImages)
                        .itemsIds(R.array.itemIds)
                        .itemsCallback(new MaterialDialog.ListCallback() {
                            @Override
                            public void onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                                switch (which){
                                    case 0:
                                        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                                        photoPickerIntent.setType("image/*");
                                        startActivityForResult(photoPickerIntent, SELECT_PHOTO);
                                        break;
                                    case 1:
                                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                        if (intent.resolveActivity(getPackageManager()) != null) {
                                            startActivityForResult(intent, CAPTURE_PHOTO);
                                        }
                                        break;
                                    case 2:
                                        mImageView.setImageResource(R.drawable.edit_image);
                                        break;
                                }
                            }
                        })
                        .show();
                break;

        }
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == SELECT_PHOTO){
            if (resultCode == RESULT_OK){
                try{
                    mImageUri = data.getData();//final Uri imageUri=data.getData();
                    final InputStream imageStream=getContentResolver().openInputStream(mImageUri);
                    final Bitmap selectedImage=BitmapFactory.decodeStream(imageStream);
                    setProgressBar();
                    mImageView.setImageBitmap(selectedImage);

                } catch (FileNotFoundException e){
                    e.printStackTrace();
                }
            }
        } else if(requestCode==CAPTURE_PHOTO){
            if(resultCode==RESULT_OK){
                onCaptureImageResult(data);

            }
        }


    }//end onActivityResult()

    private void onCaptureImageResult(Intent data) {
        thumbnail=(Bitmap)data.getExtras().get("data");
        setProgressBar();
        mImageView.setMaxWidth(200); //fixme wenn grossers Bild
        mImageView.setImageBitmap(thumbnail);
        mImageUri = data.getData();
    }


    public void addToDb(View view){
        mImageView.setDrawingCacheEnabled(true);
        mImageView.buildDrawingCache();
        Bitmap bitmap=mImageView.getDrawingCache();
        ByteArrayOutputStream baos=new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG,200,baos); //format fixme jpg, png
        byte[] data=baos.toByteArray();
        //dbHelper.addToBd(data); //fixme DB
        Toast.makeText(this, "Image saved to DB successfully", Toast.LENGTH_SHORT).show();
    }

//    //functions fuer SharedPreferences
//    public void loadData() {
//        sharedPreferences = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
//        NameHolder = sharedPreferences.getString(TEXT, "");
//        switchOnOff = sharedPreferences.getBoolean(SWITCH1, false);
//    }
//
//    public void updateViews() {
//        editTextName.setText(NameHolder);
//        switch1.setChecked(switchOnOff);
//    }

}
