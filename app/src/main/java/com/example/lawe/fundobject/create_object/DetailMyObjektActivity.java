package com.example.lawe.fundobject.create_object;

import android.Manifest;
import android.animation.Animator;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.lawe.fundobject.MainActivity;
import com.example.lawe.fundobject.R;
import com.example.lawe.fundobject.adapters_helper.SQLiteHelper;
import com.example.lawe.fundobject.home_user.HomePageUser;
import com.example.lawe.fundobject.models.Objekt;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import uk.co.senab.photoview.PhotoViewAttacher;

public class DetailMyObjektActivity extends AppCompatActivity {

    //für call to identifier permission request
    private static final int REQUEST_CALL =1 ;


    String IDholder;
    TextView id, name,kategorie, price,beschreibung,detail_objekt_Adresse,detail_objekt_date;
    ImageView image_objekt_detail;
    SQLiteDatabase sqLiteDatabase;
    SQLiteHelper sqLiteHelper;
    Cursor cursor;
    Button  btnEdit, btnAbbrechen, btnLoeschen;
    String SQLiteDataBaseQueryHolder ;
    // ScrollView scrollView;

    //persistence
    SQLiteDatabase sqLiteDatabaseObj;
    private DatabaseReference mDatabaseRef;
    private Objekt mObjekt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_my_objekt);

        // id = (TextView) findViewById(R.id.detail_objekt_name);//fixme objekt_id_update
        name = (TextView) findViewById(R.id.detail_my_objekt_name);
        kategorie = (TextView) findViewById(R.id.detail_my_objekt_kategorie);
        price = (TextView) findViewById(R.id.detail_my_objekt_price);
        detail_objekt_date = (TextView) findViewById(R.id.detail_my_objekt_date);
        beschreibung = (TextView) findViewById(R.id.detail_my_objekt_beschreibung);
        detail_objekt_Adresse = (TextView)findViewById(R.id.detail_my_objekt_Adresse);
        image_objekt_detail = (ImageView) findViewById(R.id.image_my_objekt_detail);

        btnLoeschen = (Button) findViewById(R.id.btnMyDetailLoeschen);
        btnEdit = (Button)findViewById(R.id.btnMyDetailEdit);
        btnAbbrechen = (Button)findViewById(R.id.btnMyDetailAbbrechen);

//        sqLiteHelper = new SQLiteHelper(this);
        mDatabaseRef = FirebaseDatabase.getInstance().getReference("Objekt");
        //scrollView.setVisibility(View.VISIBLE);
        // mObjekt = (Objekt) getIntent().getSerializableExtra("objektDetail");
        // getIntent().getStringExtra("objektName");

        // id.setText(getIntent().getStringExtra("objektPrice")); //fixme float , int  Price todo
        name.setText(getIntent().getStringExtra("objektName"));
        kategorie.setText(getIntent().getStringExtra("objektKategorie"));
        price.setText(getIntent().getStringExtra("objektPrice"));
        detail_objekt_date.setText(getIntent().getStringExtra("objektDate")); //todo fix date
        //detail_objekt_date.setBackgroundColor(Integer.parseInt("#b3959f"));
        beschreibung.setText(getIntent().getStringExtra("objektBeschreibung"));
        detail_objekt_Adresse.setText(getIntent().getStringExtra("objektAdresse"));

        getSupportActionBar().setTitle("Detail :  " +getIntent().getStringExtra("objektName")); // title an page
        Picasso.get()
                //.load("https://parismatch.be/app/uploads/2018/04/Macaca_nigra_self-portrait_large-e1524567086123-1100x715.jpg")
                //.load(new File(objektCurrent.getImage()))
                .load(getIntent().getStringExtra("objektImage"))
                .placeholder(R.mipmap.ic_launcher_round)
                //.error(R.mipmap.add_input_image_muster)
                .noFade().resize(150,150)
                .centerCrop()
                .into(image_objekt_detail);
        // photoViewAttacher = new PhotoViewAttacher(image_objekt_detail); local zoom

        image_objekt_detail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //function task 2 param, nameObjekt and PictureURI
                pictureZoom(getIntent().getStringExtra("objektName"),getIntent().getStringExtra("objektImage"));
            }
        });

        btnLoeschen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteObjekt(getIntent().getStringExtra("objektID"));

            }
        });

        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent_edit = new Intent(getApplicationContext(),EditMyObjektActivity.class);
                intent_edit.putExtra("objektID",getIntent().getStringExtra("objektID"));
                intent_edit.putExtra("objektImage", getIntent().getStringExtra("objektImage"));
                // intent_edit.putExtra("objektType",  mObjekts.get(position).getType);
                intent_edit.putExtra("objektName", getIntent().getStringExtra("objektName"));
                intent_edit.putExtra("objektKategorie", getIntent().getStringExtra("objektKategorie"));
                intent_edit.putExtra("objektPrice", getIntent().getStringExtra("objektPrice"));
                intent_edit.putExtra("objektDate",  getIntent().getStringExtra("objektDate"));
                intent_edit.putExtra("objektBeschreibung",getIntent().getStringExtra("objektBeschreibung"));
                intent_edit.putExtra("idErsteller",  getIntent().getStringExtra("idErsteller"));
                //intent_edit.putExtra("Phone", 123456  );
               // intent_edit.putExtra("objektAdresse",  mObjekts.get(position).getPlz().concat(mObjekts.get(position).getStrasse()));
                intent_edit.putExtra("objektPlz", getIntent().getStringExtra("objektPlz"));
                intent_edit.putExtra("objektStrasse", getIntent().getStringExtra("objektStrasse"));
                intent_edit.putExtra("objektStadt", getIntent().getStringExtra("objektStadt"));
                intent_edit.putExtra("objektDate",  getIntent().getStringExtra("objektDate"));

                startActivity(intent_edit);

            }
        });


        btnAbbrechen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(),HomeActivity.class);
                startActivity(intent);

            }
        });

    }

    private void pictureZoom(String objektName, String objektImage){
        Intent intent = new Intent(DetailMyObjektActivity.this,PictureZoom.class);
        intent.putExtra("objektName", objektName);
        intent.putExtra("objektImage",objektImage);
        startActivity(intent);

    }

    private void deleteObjekt(final String objetktID) {

        new AlertDialog.Builder(this)
                .setTitle("Delete my Object")
                .setMessage("Do you really want to do it ?")
                .setIcon(R.mipmap.ic_launcher_round)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    Intent intent;
                    public void onClick(DialogInterface dialog, int whichButton) {
                       // Toast.makeText(DetailMyObjektActivity.this, "JA", Toast.LENGTH_SHORT).show();
                        try{
                            DatabaseReference dR = FirebaseDatabase.getInstance().getReference("Objekt").child(objetktID);
                            dR.removeValue();
                            Toast.makeText(DetailMyObjektActivity.this,"Dein Anzeige wurde in DB geloescht",Toast.LENGTH_LONG).show();
                            //return Home
                           intent = new Intent(getApplicationContext(),HomePageUser.class);
                            Thread.sleep(5000);

                        }
                        catch(InterruptedException e){
                            System.out.println("main thread interrupted" +e.getMessage());
                        }

                        startActivity(intent);
                    }})
                .setNegativeButton(android.R.string.no, null).show();


    }

    //convertor Date
    public static String getDateErstellen(Date date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "EEE, d MMM yyyy HH:mm", Locale.getDefault());
        return dateFormat.format(date);
    }

}

